#pragma once
#ifndef  _PLAYSCREEN_H
#define _PLAYSCREEN_H

#include "InputManager.h"
#include "Level.h"
//#include "player.h"

using namespace SDL;

class PlayScreen: public GameObject
{
private:
	Timer * timer;
	InputManager *input;
	Level * level;
	/*Player * player1;*/
public:
	PlayScreen();
	~PlayScreen();

	void Update();
	void Render();
};

#endif // ! _PLAYSCREEN_H

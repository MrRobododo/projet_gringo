#pragma once
#ifndef _MATHHELPER_H
#define _MATHHELPER_H

#include <math.h>

#define PI 3.14159265
#define DEG_TO_RAID  PI/180.0f

namespace SDL {
	struct Vector2  //struct est propre au c, mais compar� a une class, une struct a de base toute ses propri�t� priv�
	{
		float x;
		float y;

		Vector2(float _x = 0.0f, float _y = 0.0f) :
			x(_x), y(_y) {}

		float MagnitudeSqr()
		{
			return x * x + y * y;
		}

		float Magnitude()
		{
			return (float)sqrt(x*x + y * y);
		}

		Vector2 Normalized()
		{
			float mag = Magnitude();
			return Vector2(x / mag, y / mag);
		}

		Vector2 & operator +=(const Vector2& rhs)
		{
			x += rhs.x;
			y += rhs.y;

			return *this;
		}
		Vector2 & operator -=(const Vector2& rhs)
		{
			x -= rhs.x;
			y -= rhs.y;

			return *this;
		}
	};


	inline Vector2 operator +(const Vector2& lhs, const Vector2 & rhs)
	{
		return Vector2(lhs.x + rhs.x, lhs.y + rhs.y);
	}

	inline Vector2 operator -(const Vector2& lhs, const Vector2 & rhs)
	{
		return Vector2(lhs.x - rhs.x, lhs.y - rhs.y);
	}

	inline Vector2 operator *(const Vector2& lhs, const float & rhs)
	{
		return Vector2(lhs.x * rhs, lhs.y * rhs);
	}

	inline Vector2 RotateVector(Vector2 vec, float angle)
	{
		float radAngle = (float)(angle*DEG_TO_RAID);
		return Vector2((float)vec.x*cos(radAngle) - vec.y*sin(radAngle), (float)vec.x*sin(radAngle) + vec.y*cos(radAngle));
	}

	inline Vector2 Lerp(Vector2& start, Vector2 &end, float time)
	{
		if (time <= 0.0f)
			return start;

		if (time >= 1.0f)
			return end;

		Vector2 dir = (end - start).Normalized();
		float mag = (end - start).Magnitude();

		return start + dir * mag* time;
	}

	const Vector2 VEC2_ZERO = { 0.0f, 0.0f };  // 0.0; 0.0
	const Vector2 VEC2_ONE = { 1.0f,1.0f }; // 1.0 ;1.0
	const Vector2 VEC2_UP = { 0.0f, 1.0f }; //0.0,1.0
	const Vector2 VEC2_RIGHT = { 1.0f, 0.0f }; //1.0;0.0
}
#endif // !_MATHHELPER_H

#include "GameManager.h"


namespace SDL {

	GameManager *GameManager::instance = NULL;

	GameManager * GameManager::Instance()
	{
		if (instance == NULL)
			instance = new GameManager();
		return instance;
	}

	void GameManager::Release()
	{
		delete instance;
		instance = NULL;
	}


	GameManager::GameManager()
	{
		mQuit = false;
		mGraphics = Graphics::Instance();
		if (!Graphics::Initialized())
			mQuit = true;
		mTimer = Timer::Instance();

		mSound = AudioManager::Instance();

		mAssetsManager = AssetsManager::Instance();
		mInputManager = InputManager::Instance();

		mScreenManager = ScreenManager::Instance();

		//mTex = new AnimatedTexture("player.bmp", 0, 0,50,50,8, 0.5f, AnimatedTexture::horizontal);

		//mTex->Pos(Vector2(500, 100));
		///*mTex->WrapMode(AnimatedTexture::once);*/

		//t1 = new Texture("player.bmp", 0, 0, 50, 50);
		//t1->Pos(Vector2(500, 500));

		////mTex->Scale(Vector2(3.0f, 6.0f));
		TX = new Texture("background.bmp");
		//mTex->Scale(Vector2(5.0f, 5.0f));
		///*TX = new Texture("HELLO WORLD!", "Magic_Dreams.ttf", 72, { 255,0,0,255 });*/
		//mTex->Pos(Vector2(200, 300));

		//TX->Pos(Vector2(500, 100));
	}


	GameManager::~GameManager()
	{
		AudioManager::Release();
		mSound = NULL;

		AssetsManager::Release();
		mAssetsManager = NULL;

		Graphics::Release();
		mGraphics = NULL;

		InputManager::Release();
		mInputManager = NULL;

		Timer::Release();
		mTimer = NULL;

		ScreenManager::Release();
		mScreenManager = NULL;

	}

	void GameManager::EarlyUpdate() {
		mInputManager->Update();
	}

	void GameManager::Update()
	{
		mScreenManager->Update();
	
	}

	void GameManager::LateUpdate()
	{
		mInputManager->UpdatePrevInput();
		mTimer->Reset();
	}

	void GameManager::Render()
	{
		mGraphics->ClearBackBuffer();
		mScreenManager->Render();
		mGraphics->Render();
	}

	void GameManager::Run()
	{


		while (!mQuit)
		{
			mTimer->Update();
			while (SDL_PollEvent(&mEvent) != 0) // 0 est la commande pour quit
			{
				if (mEvent.type == SDL_QUIT)
				{
					mQuit = true;
				}

			}
			if (mTimer->DeltaTimer() >= 1.0f / FRAME_RATE)
			{
				EarlyUpdate();
				Update();
				LateUpdate();

				Render();
			}

		}
	}


}
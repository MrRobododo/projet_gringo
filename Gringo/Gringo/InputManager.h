#pragma once
#ifndef _INPUTMANAGER_H
#define _INPUTMANAGER_H

#include <string.h>
#include "SDL.h"
#include "MathHelper.h"

namespace SDL {
	class InputManager
	{
	public:
		enum MOUSE_BUTTON { left = 0, right, midle, forward, backward };
		/*enum KEYINPUT{ right = 0, left, up, down};*/
	private:
		static InputManager * instance;

		Uint8 * previuosKeyState;
		const Uint8 * keyBoardStates;
		int keyLength;

		Uint32 previousMouseState;
		Uint32 mouseState;

		int mouseXPos;
		int mouseYPos;

	public:

		static InputManager * Instance();
		static void Release();

		bool KeyDown(SDL_Scancode scanCode);
		bool KeyPressed(SDL_Scancode scancode);
		bool KeyReleased(SDL_Scancode scancode);

		bool MouseButtonDown(MOUSE_BUTTON button);
		bool MouseButtonPressed(MOUSE_BUTTON button);
		bool MouseButtonReleased(MOUSE_BUTTON button);

		Vector2 MoussePos();
		void UpdatePrevInput();

		void Update();

	private:
		InputManager();
		~InputManager();
	};
}
#endif // !_INPUTMANAGER_H
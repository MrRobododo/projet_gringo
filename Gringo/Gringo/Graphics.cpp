#include "Graphics.h"


namespace SDL {

	Graphics *Graphics::instance = NULL;
	bool Graphics::init = false;

	Graphics *Graphics::Instance()
	{
		if (instance == NULL)
		{
			instance = new Graphics();
		}
		return instance;
	}


	void Graphics::Release()
	{
		delete instance;
		instance = NULL;

		init = false;

	}


	bool Graphics::Initialized()
	{
		return instance;
	}

	Graphics::Graphics() {
		//mBackBuffer = NULL;
		init = Init();
	}
	Graphics ::~Graphics()
	{
		SDL_DestroyWindow(window);
		window = NULL;

		SDL_DestroyRenderer(renderer);
		renderer = NULL;

		TTF_Quit();
		IMG_Quit();
		SDL_Quit();
	}

	bool  Graphics::Init()
	{
		if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
		{
			cout << "SDL Initializaion Error" << SDL_GetError() << endl;
			return false;
		}
		window = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
			SCREEN_WIDTH, SCREEN_HEIGHT, 0);

		if (window == NULL)
		{
			cout << "SDL Window create fail" << SDL_GetError() << endl;
			return false;
		}

		renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
		if (renderer == NULL)
		{
			cout << "Renderer creation error" << SDL_GetError() << endl;
			return false;
		}

		SDL_SetRenderDrawColor(renderer, 0x00, 0xff, 0xff, 0xff);


		//Pour plus tard, sdl a b
		/*int flags = IMG_INIT_PNG;
		if (!(IMG_Init(flags)&flags))
		{
			cout << "IMG initialization Error" << IMG_GetError() << endl;
			return false;
		}
	*/

		if (TTF_Init() == -1)
		{
			cout << "TTF initialization Error" << TTF_GetError() << endl;
			return false;
		}

		return true;
		// Use this function to get the SDL surface associated with the window. ( SDL_Surface moins rentable en SLD2)

	}

	SDL_Texture * Graphics::LoadTexture(string path)
	{
		SDL_Texture *tex = NULL;
		SDL_Surface *tempSurface = SDL_LoadBMP(path.c_str());
		if (tempSurface == NULL)
		{
			cout << "Unable to load image from" << path << endl;
			cout << "SDL_image Error: " << SDL_GetError() << endl;
			return tex;
		}
		Uint32 key = SDL_MapRGBA(tempSurface->format, 255, 0, 200,255); // enleve la couleur ffff00c8 ( 255,0,200) il faut verifier que le fichier est au format 24b / RGb
		// si on veut du 32b, on peut avec des fichier argb, il faut changer SDL_MapRGB par SDL_MapRGBA
	
		SDL_SetColorKey(tempSurface, SDL_TRUE,key); // applique les changement de surface a la surface
		if ((tex = SDL_CreateTextureFromSurface(renderer, tempSurface)) == NULL) {
			cout<< "Unable to create SDL Texture : %s : %s"<< path.c_str()<< endl;
			return false;
		}
		tex = SDL_CreateTextureFromSurface(renderer, tempSurface);
		SDL_FreeSurface(tempSurface);
		return tex;
	}

	SDL_Texture* Graphics::CreateTextTexture(TTF_Font *font, string text, SDL_Color color)
	{
		SDL_Surface * surface = TTF_RenderText_Solid(font, text.c_str(), color);
		if (surface == NULL)
		{
			cout << "Text render error" << TTF_GetError() << endl;
			return NULL;
		}
		SDL_Texture * tex = SDL_CreateTextureFromSurface(renderer, surface);
		if (tex == NULL)
		{
			cout << " Text texture creation error" << SDL_GetError() << endl;
			return NULL;
		}

		SDL_FreeSurface(surface);
		return tex;
	}

	void Graphics::ClearBackBuffer()
	{
		SDL_RenderClear(renderer);
	}

	void Graphics::DrawTexture(SDL_Texture * tex, SDL_Rect *clip, SDL_Rect* render, float angle, SDL_RendererFlip flip)
	{
		SDL_RenderCopyEx(renderer, tex, clip, render, angle, NULL, flip);
	}

	void Graphics::Render()
	{
		SDL_RenderPresent(renderer);
	}
}
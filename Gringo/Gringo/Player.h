
#ifndef PLAYER_H
#define PLAYER_H


#include <iostream>
#include <vector>

#include "InputManager.h"
#include "AnimatedTexture.h"
#include "AudioManager.h"

static int number_player;
using namespace std;

//constante du char size

using namespace SDL;

class Player : public GameObject
{
private:

	Timer * timer;
	InputManager * input;

	Texture * playerTex;
	AnimatedTexture *playerWalking;

	SDL_Rect destRect;

	bool bVisible;
	bool bAnimation;
	bool bMoving;
	bool bDirection;

	int lives;

	
	bool bJumping = false;
	bool bFalling;

	float offset_X;

	float speedX = 3;

	int speedY = 5;
	//float speedY = 15.0f;
	float jump_high = 150;

	int MAX_FALL_SPEED = 10;
	float gravity =5.0f;


	long flags;

	float WALL_LEFT;
	float WALL_RIGHT;
	float GROUND_Y;
	
	int walkingHeight;
	
public:

	static const int playerSize_W = 50;
	static const int playerSize_H = 50;

	bool bOnGround;

	bool bWasOnGround;

	float xVelocity;
	float yVelocity;

	bool bCollide;

	bool bCollisionRight;
	bool bCollisionLeft;
	bool bcornerBottomRight;

	

public:
	Player();
	~Player();

	void MoveToWordl(vector< Texture*> tile_list);

	void Set_camera(SDL_Rect *camera, Player *player, int LEVEL_WIDTH, int LEVEL_HEIGHT);


	bool Collssion( SDL_Rect  , Texture*);

	void RenderState();
	void HandleState();
	
	bool IsAnimating();
	void WasHit();


	int DeplaceText(Texture*, int x, int y);

	void Update();
	void Render(SDL_Rect);

private:


	int EssaieDeplacement(Texture*, int vx, int vy);

	void Affine(Texture*, int x, int y);

};
#endif // PLAYER_H


//class player
//{	
//	// Variable personnage 
//	int health;
//
//	// Variable de déplacemnt 
//	int delay = 6;
//	int xvel, yvel;			// xvel vitesse déplacement horizontal  || yvel vitesse déplacement verticale
//	char direction;				// Direction de déplacement du personnage ( l -> left || r -> right)
//	bool moving;
//
//	bool onGround;
//	bool wasOnGround;
//
//	int jump_high = 150;
//	int up_v = 5;
//	int character_VEL = 3;
//
//	int move_x, move_y;
//
//	
//	// Emplacement affichage du personnage
//	static const int PLAYER_Width = 50;
//	static const int PLAYER_Height = 50;
//	SDL_Texture* image;			// Image lié au personnage
//	SDL_Rect clips[40];			// Tableau de stockages de chaque frame du personnage
//	double frame;				// Float qui nous indique qu'elle image choisir pour l'animation en cours
//
//	SDL_Rect hitbox = { box.x - 1,box.y - 1,box.x + box.w - 1, box.y + box.h - 1 }; // hit box , x, y w, h
//	SDL_Rect hurtbox;
//
//public:
//
//	// Constructeur// Destructeur
//	player(SDL_Rect box, SDL_Texture*,SDL_Renderer* renderer);
//	~player();
//	//variables 
//	SDL_Rect box;
//
//	/*Declaration des fonctions du.cpp */	
//	// Affichage du personnage
//	void show(SDL_Renderer* renderer, char dir, SDL_Rect *);
//	void render(SDL_Renderer* gRenderer, SDL_Rect* camera);
//	// Methodes pour le deplcament du personnage
//	//void move(const std::vector<std::vector<int>> &map);
//	void move( int **, int, int );
//	//void move_bullet(int **bg);
//	void handle_event(int **, SDL_Event &e,SDL_Renderer *);
//	
//	//Section get et set dans le header qui peuvent être deffini directement ici
//	SDL_Rect* getRect() { return &box; }
//	double getYvel() {	return  yvel;	}
//	double getXvel() {	return xvel;	}
//	void setXvel(double vel) {	xvel = vel;	}
//
//	void set_camera(SDL_Rect *camera, player *player, int LEVEL_WIDTH, int LEVEL_HEIGHT)
//	{
//		camera->x = (player->box.x + player->box.w / 2) - camera->w / 2;
//		camera->y = (player->box.y + player->box.h / 2) - camera->h / 2;
//		if (camera->x < 0) {
//			camera->x = 0;
//		}
//		if (camera->y < 0) {
//			camera->y = 0;
//		}
//		//if (camera->x > LEVEL_WIDTH - camera->w ) {
//		//	cout << "call";
//		//	camera->x = LEVEL_WIDTH - camera->w;
//		//}
//		//if (camera->y >  LEVEL_HEIGHT - camera->h) {
//		//	camera->y = LEVEL_HEIGHT - camera->h;
//		//}      
//		if (camera->x + camera->w > LEVEL_WIDTH) {
//			camera->x = LEVEL_WIDTH - camera->w;
//		}
//		if (camera->y + camera->h > LEVEL_HEIGHT) {
//			camera->y = LEVEL_HEIGHT - camera->h;
//		}
//	}
//
//	int getHealth()	{return health;	}
//	void setHealth(int h){	health = h;	}
//	
//	void setMoving(bool b)	{	moving = b;	}
//	char getDirection() {	return direction;}
//	void setDirection(char dir)	{
//		if ((dir == 'r' || dir == 'l') && direction != dir)
//			direction = dir;
//	}
//	//bool getJump() {	return jump;	}
//	void setJump()	{
////		// Gestion du saut
////		if (ground && !jump)
////		{
////			jump = 1;
//////			ground = 0;
////			yvel = -15;		// Vitesse de deplacement vertical lorsque l'on appuis sur le bouton
////			box.y -= 6;		// Vitesse de chute
////		}
//	}	
//
//	// the three function below will check if the 
//	// the character will go out of the boundary
//	// or is empty. Notice that if it is not empty,
//	// the character will not move.
//
//	bool on_the_ground(int**bg) {
//		for (int i = box.x; i < box.w + box.x; i++) {
//			// notice that the object is actually
//			// place.x ~ place.x + place.w - 1
//			// place.y ~ place.y + place.h - 1
//			// in bg, so place.y + place.h
//			// is the next pixel under the object
//			if (bg[i][box.y + box.h] == ground ) {
//				return true;
//			}
//		}
//		return false;
//	}
//
//
//	bool check_top(int**bg) {
//		//// check if out of bound
//		int bound = box.y - 1;
//		if (bound < 0)
//			return false;
//		// check empty
//		for (int i = box.x; i < box.x + box.w; i++) {
//			if (bg[i][bound] != vide)
//			{
//				if (bg[i][bound] == ground2){ return true;  }
//					
//				return false;
//			}
//		}
//		///*cout << "collision 3" << endl;*/
//		return true;
//	}
//
//	
//	bool check_left(int** bg) {
//		//// when going left, you need to make sure that
//		//// the character will not go out of the bg
//		int bound = box.x - 1;
//		//int bound = hitbox.x;
//		if (bound < 0)
//			return false;
//		// check if empty
//		for (int i = box.y; i < box.y + box.h; i++) {
//			if (bg[bound][i] != vide)  {
//				if (bg[bound][i] == ground2)
//				{
//					return true;
//				}
//				if (bg[bound][i] == monster)
//				{
//
//				}
//			return false;
//			}
//		}
//
//		return true;
//	}
//	
//	
//	bool check_right(int** bg,  int WIDTH) {
//		//// when going right, you need to make sure that
//		//// the character will not go out of the bg
//		int bound = box.x + box.w;
//		/*cout << "box x : " << box.x << "||" << "box x+ w : " << box.x + box.y << endl;*/
//		if (bound >= WIDTH)
//		{	
//			cout << "end of the level";
//			return false;
//		}
//		// check if empty
//		for (int i = box.y; i < box.y + box.h; i++) {
//			if (bg[bound][i] != vide) {
//				if (bg[bound][i] == ground2)
//				{
//					return true;
//				}
//				return  false;
//			}
//		}		
//		return true;
//	}
//	
//	
//
//	bool check_top_collision(int **, SDL_Rect )
//	{
//		/*if (checkCollision(box, b[i]))*/
//	}
//
//};
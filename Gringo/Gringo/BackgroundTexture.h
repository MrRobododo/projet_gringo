#pragma once
#ifndef _BACKGROUNDTEXTURE_H
#define _BACKGROUNDTEXTURE_H

#include "Timer.h"
#include "Texture.h"
#include "LoadMap.h";

using namespace SDL;
class BackgroundTexture : public Texture
{
	static bool bScrool;
	Timer * timer;

	Vector2 startPos;
	float scrollSpeed;
	int scrollingOffset ;

public:
	BackgroundTexture(SDL_Rect camera);
	~BackgroundTexture();

	void Update();
	void Render(SDL_Rect camera, LoadMap* map);

private:
	void ScrollSky();
};

#endif // !_BACKGROUNDTEXTURE_H
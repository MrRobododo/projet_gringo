#pragma once
#ifndef _ASSETSMANAGER_H
#define _ASSETSMANAGER_H

#include <map>
#include"SDL_mixer.h"
#include "Graphics.h"

using namespace std;

namespace SDL {
	class AssetsManager
	{
	private:
		static AssetsManager * instance;

		map<string, SDL_Texture*> textures_list;
		map< string, SDL_Texture*> text_list;
		map< string, TTF_Font *> font_list;
		map<string, Mix_Music*>  music_list;
		map<string, Mix_Chunk*> sfx_sound_list;

	public:
		static AssetsManager * Instance();
		static void Release();

		SDL_Texture* GetTexture(string filename);
		SDL_Texture* GetText(string text, string filename, int size, SDL_Color color);

		Mix_Music* GetMusic(string filename);
		Mix_Chunk* GetSFX(string filename);

	private:
		AssetsManager();
		~AssetsManager();

		TTF_Font * GetFont(string filename, int size);
	};

}
#endif // !_ASSETSMANAGER_H
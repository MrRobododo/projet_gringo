#include "Texture.h"

namespace SDL {

	Texture::Texture(string filename)
	{
		graphics = Graphics::Instance();

		tex = AssetsManager::Instance()->GetTexture(filename);

		SDL_QueryTexture(tex, NULL, NULL, &width, &height);

		clipped = false;

		renderRect.w = width;
		renderRect.h = height;
	}

	Texture::Texture(string filename, int x, int y, int w, int h)
	{
		graphics = Graphics::Instance();

		tex = AssetsManager::Instance()->GetTexture(filename);

		clipped = true;
		pos.x = x;
		pos.y = y;

		width = w;
		height = h;

	//	rect = { pos.x, pos.y, width, height };

		clipRect.x = 0;
		clipRect.y = 0;
		clipRect.w = width;
		clipRect.h = height;
	}


	Texture::Texture(string filename, int x, int y, int w, int h, bool cliped)
	{
		graphics = Graphics::Instance();

		tex = AssetsManager::Instance()->GetTexture(filename);

		clipped = cliped;
		pos.x = x;
		pos.y = y;

		width = w;
		height = h;

		//rect = { pos.x, pos.y, width, height };

		clipRect.x = 0;
		clipRect.y = 0;
		clipRect.w = TILE_W_PXL;
		clipRect.h = TILE_H_PXL;
	}

	Texture::Texture(string filename, int  posX, int posY, int w, int h, int evt, int id, int nb)
	{
		graphics = Graphics::Instance();
		tex = AssetsManager::Instance()->GetTexture(filename);

		clipped = true;
		pos.x = posX;
		pos.y = posY;
		width = w;
		height = h;

		//rect = { pos.x, pos.y, width, height };

		type = evt;
		reference = nb;

		int x=0 , y=0 ;
		int tmp = id;
		int nb_collone = 4;
			for (int i = 0; i <= tmp; i++) {
				if (i = tmp)
				{
					/*fprintf(stdout, "%d/%d ", i / nb_collone, i % nb_collone);*/
					x = i / nb_collone;
					y = i % nb_collone;
				}
				if (i % nb_collone == nb_collone - 1) {
				}
			}
		clipRect.x = y * TILE_W_PXL;
		clipRect.y = x * TILE_H_PXL;
		//clipRect.x = id* TILE_W_PXL;
		//clipRect.y = 0;
		clipRect.w = TILE_W_PXL;
		clipRect.h = TILE_H_PXL;
	}

	Texture::Texture(string text, string fontpath, int size, SDL_Color color)
	{
		graphics = Graphics::Instance();
		tex = AssetsManager::Instance()->GetText(text, fontpath, size, color);

		clipped = false;

		SDL_QueryTexture(tex, NULL, NULL, &width, &height);

		renderRect.w = width;
		renderRect.h = height;
	}



	Texture::~Texture()
	{
		tex = NULL;
		graphics = NULL;
	}

	void Texture::Render()
	{
		Vector2 pos = Pos(world);
		Vector2 scale = Scale(world);

		renderRect.x = (int)(pos.x - width * scale.x * 0.5f);

		renderRect.y = (int)(pos.y - height * scale.y * 0.5f);

		renderRect.w = (int)(width *scale.x);
		renderRect.h = (int)(height * scale.y);

		graphics->DrawTexture(tex, (clipped) ? &clipRect : NULL, &renderRect, Rotation(world));
	}

	void Texture::Render(SDL_Rect *camera)
	{
		Vector2 pos = Pos(world);
		Vector2 scale = Scale(world);
		renderRect.x = (int)(pos.x - width * scale.x * 0.5f) - camera->x;

		renderRect.y = (int)(pos.y - height * scale.y * 0.5f) - camera->y;
		renderRect.w = width;
		renderRect.h = height;
		graphics->DrawTexture(tex, &clipRect, &renderRect, Rotation(world));
	}


	void Texture::Render(SDL_Rect *camera, SDL_RendererFlip flip)
	{
		Vector2 pos = Pos(world);
		Vector2 scale = Scale(world);
		renderRect.x = (int)(pos.x - width * scale.x * 0.5f) - camera->x;

		renderRect.y = (int)(pos.y - height * scale.y * 0.5f)-camera->y;
		renderRect.w = width;
		renderRect.h = height;
		graphics->DrawTexture(tex, &clipRect, &renderRect, Rotation(world), flip);
		/*SDL_RenderCopyEx(gRenderer, picture, &source_Rect, &dest_Rect, 0.0, NULL, SDL_FLIP_NONE);*/
	}
}
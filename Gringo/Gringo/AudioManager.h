#pragma once
#ifndef _AUDIOMANAGER_H
#define _AUDIOMANAGER_H

#include "AssetsManager.h"

namespace SDL {
	class AudioManager
	{
	private:
		static AudioManager* instance;
		AssetsManager* assetMgr;
	public:

		static AudioManager * Instance();
		static void Release();

		void PlayMusic(string filename, int loops = -1);
		void PauseMusic();
		void ResumeMusic();

		void PlaySFX(string filename, int loops = 0, int channel = 0);

	private:
		AudioManager();
		~AudioManager();
	};
}

#endif // !_AUDIOMANAGER_H
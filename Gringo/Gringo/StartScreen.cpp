#include "StartScreen.h"

using namespace SDL;

StartScreen::StartScreen()
{
	mTimer = Timer::Instance();
	input = InputManager::Instance();

	topBar = new GameObject(Vector2(Graphics::Instance()->SCREEN_WIDTH * 0.5f, 10.0f));

	playerOne = new Texture("1 UP", "Magic_Dreams.ttf", 32, { 200,0,0 });
	playerTwo = new Texture("2 UP", "Magic_Dreams.ttf", 32, { 200,0,0 });
	highScore = new Texture("HI-SCRORE", "Magic_Dreams.ttf", 32, { 200,0,0 });
	playerOneScore = new Scoreboard();
	playerTwoScore = new Scoreboard();
	topHighScore = new Scoreboard();

	playerOne->Parent(topBar);
	playerTwo->Parent(topBar);
	highScore->Parent(topBar);
	playerOneScore->Parent(topBar);
	playerTwoScore->Parent(topBar);
	topHighScore->Parent(topBar);


	playerOne->Pos(Vector2(-Graphics::Instance()->SCREEN_WIDTH*0.35f, 0.0f));;
	playerTwo->Pos(Vector2(Graphics::Instance()->SCREEN_WIDTH*0.35f, 0.0f));;
	highScore->Pos(Vector2(-30.0f, 0.0f));

	playerOneScore->Pos(Vector2(-Graphics::Instance()->SCREEN_WIDTH*0.23f, 40.0f));;
	playerTwoScore->Pos(Vector2(Graphics::Instance()->SCREEN_WIDTH*0.42f, 40.0f));;
	topHighScore->Pos(Vector2(Graphics::Instance()->SCREEN_WIDTH*0.23f, 40.0f));

	topHighScore->Score(30000);

	topBar->Parent(this);

	// Play mode entities
	playModes = new GameObject(Vector2(Graphics::Instance()->SCREEN_WIDTH*0.5f, Graphics::Instance()->SCREEN_HEIGHT*0.55f));
	onePlayerMode = new Texture("1 Player", "Magic_Dreams.ttf", 32, { 230, 230, 230 });
	twoPlayerMode = new Texture("2 Players", "Magic_Dreams.ttf", 32, { 230, 230, 230 });
	cursor = new Texture("cursor.bmp");

	onePlayerMode->Parent(playModes);
	twoPlayerMode->Parent(playModes);
	cursor->Parent(playModes);

	onePlayerMode->Pos(Vector2(-19.0f, -35.0f));
	twoPlayerMode->Pos(Vector2(0.0f, 35.0f));
	cursor->Pos(Vector2(-170.0f, -35.0f));

	playModes->Parent(this);

	cursorStartPos = cursor->Pos(local);
	cursorOffset = Vector2(0.0f, 70.0f);
	selectedMode = 0;



	//Bottom bar
	botBar = new GameObject(Vector2(Graphics::Instance()->SCREEN_WIDTH*0.5f, Graphics::Instance()->SCREEN_HEIGHT*0.75f));
	entreprise = new Texture("entreprise", "Magic_Dreams.ttf", 32, { 200, 0, 0 });
	dates = new Texture("2020 RE - RM", "Magic_Dreams.ttf", 32, { 200,230,230 });
	rightsUser = new Texture("ALL RIGHTS RESERVED", "Magic_Dreams.ttf", 32, { 220,22 });


	entreprise->Parent(botBar);
	dates->Parent(botBar);
	rightsUser->Parent(botBar);

	botBar->Parent(this);

	entreprise->Pos(VEC2_ZERO);
	dates->Pos(Vector2(0.0f, 90.0f));
	rightsUser->Pos(Vector2(0.0f, 170.0f));

	//logo
	logo = new Texture("", 0, 0, 0, 0);
	animatedLogo = new AnimatedTexture("", 0, 0, 0, 0, 3, 1.0f, AnimatedTexture::vertical);
	logo->Parent(this);
	animatedLogo->Parent(this);


	logo->Pos(Vector2(Graphics::Instance()->SCREEN_WIDTH*0.5f, Graphics::Instance()->SCREEN_HEIGHT*0.4f));
	animatedLogo->Pos(Vector2(Graphics::Instance()->SCREEN_WIDTH*0.5f, Graphics::Instance()->SCREEN_HEIGHT*0.4f));

	ResetAnimation();
}


StartScreen::~StartScreen()
{
	//top bar
	delete topBar;
	topBar = NULL;

	delete playerOne;
	playerOne = NULL;

	delete playerTwo;
	playerTwo = NULL;

	delete highScore;
	highScore = NULL;

	delete playerOneScore;
	playerOneScore = NULL;
	
	delete playerTwoScore;
	playerTwoScore = NULL;

	delete topHighScore;
	topHighScore = NULL;

	// Play mode
	delete playModes;
	playModes = NULL;

	delete onePlayerMode;
	onePlayerMode = NULL;

	delete twoPlayerMode;
	twoPlayerMode = NULL;

	delete cursor;
	cursor = NULL;

	//Bottom bar
	delete  botBar;
	botBar = NULL;

	delete entreprise;
	entreprise = NULL;

	delete dates;
	dates = NULL;

	delete rightsUser;
	rightsUser = NULL;

	//logo
	delete logo;
	logo = NULL;

	delete animatedLogo;
	animatedLogo = NULL;
}

void StartScreen::ResetAnimation()
{

	// Screen aniamation 
	animationStartPos = Vector2(0.0f, Graphics::Instance()->SCREEN_HEIGHT);
	animationEndPos = VEC2_ZERO;

	animationTotalTime = 5.0f;
	animationTimer = 0.0f;
	animationDonne = false;


	Pos(animationStartPos);
}

int StartScreen::SelectedMode()
{
	return selectedMode;

}

void StartScreen::ChangeSelectedMode(int change)
{
	selectedMode += change;

	if (selectedMode < 0)
		selectedMode = 1;
	else if (selectedMode > 1)
		selectedMode = 0;

	cursor->Pos(cursorStartPos + cursorOffset * selectedMode);
}

void StartScreen::Update()
{
	if (!animationDonne)
	{
		animationTimer += mTimer->DeltaTimer();
		Pos(Lerp(animationStartPos, animationEndPos, animationTimer / animationTotalTime));

		if (animationTimer >= animationTotalTime)
			animationDonne = true;

		if (input->KeyPressed(SDL_SCANCODE_DOWN) || input->KeyPressed(SDL_SCANCODE_UP))
			animationTimer = animationTotalTime;
	}
	else {
		animatedLogo->Update();
		if (input->KeyPressed(SDL_SCANCODE_DOWN))
			ChangeSelectedMode(1);
		else if (input->KeyPressed(SDL_SCANCODE_UP))
			ChangeSelectedMode(-1);
	}
}

void StartScreen::Render()
{
	playerOne->Render();
	playerTwo->Render();
	highScore->Render();

	playerOneScore->Render();
	playerTwoScore->Render();
	topHighScore->Render();


	onePlayerMode->Render();
	twoPlayerMode->Render();
	cursor->Render();

	entreprise->Render();
	dates->Render();
	rightsUser->Render();

	if (!animatedLogo)
		logo->Render();
	else
		animatedLogo->Render();
}
#include "InputManager.h"

namespace SDL {
	InputManager * InputManager::instance = NULL;


	InputManager* InputManager::Instance()
	{
		if (instance == NULL)
			instance = new InputManager();
		return instance;
	}

	void InputManager::Release()
	{
		delete instance;
		instance = NULL;
	}

	InputManager::InputManager()
	{
		keyBoardStates = SDL_GetKeyboardState(&keyLength);
		previuosKeyState = new Uint8[keyLength];
		memcpy(previuosKeyState, keyBoardStates, keyLength);
	}


	InputManager::~InputManager()
	{
		delete[] previuosKeyState;
		previuosKeyState = NULL;
	}


	bool InputManager::KeyDown(SDL_Scancode scanCode)
	{
		return keyBoardStates[scanCode];
	}

	bool InputManager::KeyPressed(SDL_Scancode scancode) {
		return !previuosKeyState[scancode] && keyBoardStates[scancode];
	}

	bool InputManager::KeyReleased(SDL_Scancode scancode)
	{
		return previuosKeyState[scancode] && !keyBoardStates[scancode];
	}

	Vector2 InputManager::MoussePos()
	{
		return Vector2((float)mouseXPos, (float)mouseYPos);
	}

	bool InputManager::MouseButtonDown(MOUSE_BUTTON button)
	{
		Uint32 mask = 0;
		switch (button)
		{
		case InputManager::left:
			mask = SDL_BUTTON_LMASK;
			break;
		case InputManager::right:
			mask = SDL_BUTTON_RMASK;
			break;
		case InputManager::midle:
			mask = SDL_BUTTON_MMASK;
			break;
		case InputManager::forward:
			mask = SDL_BUTTON_X2MASK;
			break;
		case InputManager::backward:
			mask = SDL_BUTTON_X1MASK;
			break;
		default:
			break;
		}
		return (mouseState & mask);
	}

	bool InputManager::MouseButtonPressed(MOUSE_BUTTON button)
	{
		Uint32 mask = 0;
		switch (button)
		{
		case InputManager::left:
			mask = SDL_BUTTON_LMASK;
			break;
		case InputManager::right:
			mask = SDL_BUTTON_RMASK;
			break;
		case InputManager::midle:
			mask = SDL_BUTTON_MMASK;
			break;
		case InputManager::forward:
			mask = SDL_BUTTON_X2MASK;
			break;
		case InputManager::backward:
			mask = SDL_BUTTON_X1MASK;
			break;
		default:
			break;
		}
		return !(previousMouseState &mask) && (mouseState & mask);
	}

	bool InputManager::MouseButtonReleased(MOUSE_BUTTON button)
	{
		Uint32 mask = 0;
		switch (button)
		{
		case InputManager::left:
			mask = SDL_BUTTON_LMASK;
			break;
		case InputManager::right:
			mask = SDL_BUTTON_RMASK;
			break;
		case InputManager::midle:
			mask = SDL_BUTTON_MMASK;
			break;
		case InputManager::forward:
			mask = SDL_BUTTON_X2MASK;
			break;
		case InputManager::backward:
			mask = SDL_BUTTON_X1MASK;
			break;
		default:
			break;
		}
		return (previousMouseState &mask) && !(mouseState & mask);
	}

	void InputManager::UpdatePrevInput() {
		memcpy(previuosKeyState, keyBoardStates, keyLength);
		previousMouseState = mouseState;
	}


	void InputManager::Update()
	{
		mouseState = SDL_GetMouseState(&mouseXPos, &mouseYPos);
		keyBoardStates = SDL_GetKeyboardState(NULL);
	}
}
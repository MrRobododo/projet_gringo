#include "GameObject.h"


namespace SDL {
	GameObject::GameObject(Vector2 postition)
	{
		pos = postition;

		rotation = 0.0f;

		active = true;
		parent = NULL;

		scale = VEC2_ONE;
	}

	GameObject::~GameObject()
	{
		parent = NULL;
	}

	void GameObject::Pos(Vector2 Pos)
	{
		pos = Pos;
	}
	Vector2 GameObject::Pos(SPACE space)
	{
		if (space == local || parent == NULL)
			return pos;

		Vector2 parenScale = parent->Scale(world);
		Vector2 rotPos = RotateVector(Vector2(pos.x*parenScale.x, pos.y*parenScale.y), parent->Rotation(local));

		return parent->Pos(world) + rotPos;
	}

	void GameObject::Rotation(float r)
	{
		rotation = r;
		if (rotation > 360.0f)
			rotation -= 360.0f;

		if (rotation < 0.0f)
			rotation += 360.0f;
	}

	float GameObject::Rotation(SPACE space)
	{
		if (space == local || parent == NULL)
			return rotation;

		return parent->Rotation(world) + rotation;
	}

	void GameObject::Scale(Vector2 scl)
	{
		scale = scl;
	}

	Vector2 GameObject::Scale(SPACE space)
	{
		if (space == local || parent == NULL)
			return scale;

		Vector2 scl = parent->Scale(world);
		scl.x *= scale.x;
		scl.y *= scale.y;
		return scl;
	}

	void GameObject::Active(bool act)
	{
		active = act;
	}

	bool GameObject::Active()
	{
		return active;
	}

	void GameObject::Parent(GameObject* part)
	{
		if (parent == NULL)
		{
			pos = Pos(world);
			rotation = Rotation(world);
			scale = Scale(world);
		}
		else {
			if (parent != NULL)
				Parent(NULL);

			Vector2 parentScale = part->Scale(world);

			pos = RotateVector(Pos(world) - part->Pos(world), -part->Rotation(world));
			pos.x /= parentScale.x;
			pos.y /= parentScale.y;

			rotation -= part->Rotation(world);

			scale = Vector2(scale.x / parentScale.x, scale.y / parentScale.y);
		}
		parent = part;
	}

	GameObject *GameObject::Parent()
	{
		return parent;
	}

	void GameObject::Translate(Vector2 vec)
	{
		pos += vec;
	}

	void GameObject::Rotate(float amount)
	{
		rotation += amount;
	}

	void GameObject::Update()
	{

	}

	void GameObject::Render()
	{

	}
}
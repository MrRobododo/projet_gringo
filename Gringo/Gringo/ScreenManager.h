#pragma once

#ifndef _SCREENMANAGER_H 
#define _SCREENMANAGER_H
#include "StartScreen.h"
#include "Background.h"
#include "PlayScreen.h"

class ScreenManager
{
private:

	enum SCREENS {start, play};

	static ScreenManager * instance;

	InputManager * input;

	//StartScreen *startscren;
	/*Background * background;*/
	PlayScreen * playscren;

	SCREENS currentScreen;

public:

	static ScreenManager * Instance();
	static void Release();

	void Update();
	void Render();

private:
	ScreenManager();
	~ScreenManager();
};


#endif // !_SCREENMANAGER_H 
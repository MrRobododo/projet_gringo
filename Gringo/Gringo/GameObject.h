#pragma once
#ifndef _GAMEOBJECT_H
#define _GAMEOBJECT_H

#include "MathHelper.h"

namespace SDL {
	class  GameObject
	{
	public:
		enum  SPACE
		{
			local = 0, world = 1
		};

	private:
		/*Vector2 pos;*/
		float rotation;
		Vector2 scale;

		bool active;
		GameObject * parent;


	public:
		Vector2 pos;

	public:
		GameObject(Vector2 pos = VEC2_ZERO);
		~GameObject();


		void  Pos(Vector2 pos);
		Vector2 Pos(SPACE space = world);

		void Rotation(float rotation);
		float Rotation(SPACE space = world);

		void Scale(Vector2 scale);
		Vector2 Scale(SPACE space = world);

		void Active(bool active);
		bool Active();

		void Parent(GameObject* parent);
		GameObject *Parent();

		void Translate(Vector2 vec);
		void Rotate(float angle);

		virtual void Update();
		virtual void Render();

	};
}



#endif // !_GAMEOBJECT_H

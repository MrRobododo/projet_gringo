#include "Level.h"



Level::Level(int stg)
{

	stages = stg;

	camera.x = 0;
	camera.y = 0;
	camera.w = Graphics::Instance()->SCREEN_WIDTH;
	camera.h = Graphics::Instance()->SCREEN_HEIGHT;

	background = new BackgroundTexture(camera);

	map1 = new LoadMap(camera);
	player1 = new Player();

}


Level::~Level()
{
	timer = NULL;
	/*Background::Released();*/
	delete background;
	background = NULL;

	delete map1;
	map1 = NULL;

	delete player1;
	player1 = NULL;
}

void Level::MoveToWorld(Texture * go, float dx, float dy)
{
	int mx, my, hit, adj;
	if (dx != 0)
	{
		go->pos.x += dx;

		mx = dx > 0 ? (go->pos.x + go->width) : go->pos.x;
		mx /= map1->TILE_WIDTH;

		my=(go->pos.y / map1->TILE_HEIGHT);

		hit = 0;

		//if ()
	}
}


void Level::Scroll()
{
	//if (player1->pos.x < 100)
	//{
	//	player1->pos.y = 100;
	//	map1->Scroll(player1->xVelocity,0);
	//}
	//if (player1->pos.x > Graphics::Instance()->SCREEN_WIDTH/2)
	//{
	//	printf("on scroll \n");
	//	player1->pos.x = Graphics::Instance()->SCREEN_WIDTH/2;
	//	map1->Scroll(-player1->xVelocity,0);
	//}
	//if (player1->pos.y < 100)
	//{
	//	player1->pos.y = 100;
	//	map1->Scroll(0, player1->yVelocity);
	//}
	//if (player1->pos.y > Graphics::Instance()->SCREEN_HEIGHT -100)
	//{
	//	player1->pos.y = Graphics::Instance()->SCREEN_HEIGHT - 100;
	//	map1->Scroll(0, -player1->yVelocity);
	//}

}


void Level::Update()
{
	player1->Update();
	player1->MoveToWordl(map1->tile_list);
	/*player1->Move(map1->tile_list, map1->LEVEL_WIDTH, map1->LEVEL_HEIGHT);*/

	Scroll();
	//for (int i = 0; i < map1->tile_list.size(); i++)
	//{
	//	player1->DeplaceText(map1->tile_list[i]);
	//}
//	player1->Set_camera(&camera, player1, map1->LEVEL_WIDTH, map1->LEVEL_HEIGHT);
}

void Level::Render()
{
	for (int i = 0; i < map1->tile_list.size();i++)
	{
		map1->tile_list[i];
	}
	background->Render(camera, map1);

	//player1->Set_camera(&camera, player1, map1->LEVEL_WIDTH, map1->LEVEL_HEIGHT);

	map1->Render();

	player1->Render(camera);
}
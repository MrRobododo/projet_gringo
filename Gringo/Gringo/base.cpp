#include <iostream>
#include "base.h"

using namespace std;

//void baseclass::init_background(SDL_Window ** window, SDL_Renderer ** renderer , int  SCREEN_WIDTH, int SCREEN_HEIGHT)
//{
//	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
//		cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << endl;
//		exit(1);
//	}
//
//	*window = SDL_CreateWindow("Project Gringo", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
//		SCREEN_WIDTH, SCREEN_HEIGHT, 0);
//	if (*window == NULL)
//	{
//		cout << "SDL Window create fail" << endl;
//		exit(1);
//	}
//
//	*renderer = SDL_CreateRenderer(*window, -1, SDL_RENDERER_ACCELERATED);
//}


SDL_Texture *baseclass::load_img(const char *img, SDL_Renderer* renderer)
{
	SDL_Surface *tempSurface = SDL_LoadBMP(img);
	if (tempSurface == NULL)
	{
		cout << "Unable to load image from" << img << endl;
		cout << "SDL_image Error: " << SDL_GetError() << endl;
		return 0;
	}
	//Uint32 key = SDL_MapRGB(tempSurface->format, 255, 0, 200); // enleve la couleur ffff00c8 ( 255,0,200) il faut verifier que le fichier est au format 24b / RGb
	// si on veut du 32b, on peut avec des fichier argb, il faut changer SDL_MapRGB par SDL_MapRGBA

	//SDL_SetColorKey(tempSurface, SDL_TRUE,key); // applique les changement de surface a la surface

	SDL_Texture *tex = SDL_CreateTextureFromSurface(renderer, tempSurface);
	SDL_FreeSurface(tempSurface);
	return tex;
}


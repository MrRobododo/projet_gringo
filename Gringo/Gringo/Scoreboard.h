#pragma once
#ifndef _SCOREBOARD_H
#define _SCOREBOARD_H
#include "Texture.h"
#include <vector>

using namespace std;

using namespace SDL;

class Scoreboard : public GameObject
{
private:
	vector < Texture * > score;
public:
	Scoreboard();
	~Scoreboard();

	void Score(int score);
	void Render();

private:
	void ClearBoard();

};

#endif // !_SCOREBOARD_H
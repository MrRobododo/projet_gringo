#include "AssetsManager.h"

namespace SDL {
	AssetsManager *AssetsManager::instance = NULL;
	AssetsManager* AssetsManager::Instance()
	{
		if (instance == NULL)
			instance = new AssetsManager();

		return instance;
	}

	void AssetsManager::Release()
	{
		delete instance;
		instance = NULL;
	}


	AssetsManager::AssetsManager()
	{
	}

	AssetsManager::~AssetsManager()
	{
		for (auto tex : textures_list)
		{
			if (tex.second != NULL)
			{
				SDL_DestroyTexture(tex.second);
			}
		}
		textures_list.clear();

		for (auto text : text_list)
		{
			if (text.second != NULL)
			{
				SDL_DestroyTexture(text.second);
			}
		}
		text_list.clear();
		for (auto font : font_list)
		{
			if (font.second != NULL)
			{
				TTF_CloseFont(font.second);
			}
		}
		font_list.clear();


		for (auto music : music_list)
		{
			if (music.second != NULL)
			{
				Mix_FreeMusic(music.second);
			}
		}
		music_list.clear();

		for (auto sfx : sfx_sound_list)
		{
			if (sfx.second != NULL)
			{
				Mix_FreeChunk(sfx.second);
			}
		}
		sfx_sound_list.clear();
	}


	SDL_Texture * AssetsManager::GetTexture(string filename)
	{

		string fullPath = SDL_GetBasePath();
		fullPath.append("assets/" + filename);

		if (textures_list[fullPath] == nullptr)
			textures_list[fullPath] = Graphics::Instance()->LoadTexture(fullPath);

		return textures_list[fullPath];
	}

	TTF_Font* AssetsManager::GetFont(string filename, int size)
	{
		string fullPath = SDL_GetBasePath();
		fullPath.append("assets/" + filename);
		string key = fullPath + (char)size;

		if (font_list[key] == nullptr)
		{
			font_list[key] = TTF_OpenFont(fullPath.c_str(), size);
			if (font_list[key] == nullptr)
			{
				cout << "Font loading Error" << filename.c_str() << TTF_GetError();
			}
		}
		return font_list[key];
	}

	SDL_Texture* AssetsManager::GetText(string text, string filename, int size, SDL_Color color)
	{
		TTF_Font * font = GetFont(filename, size);

		if (font == nullptr) {
			cout << "font is empty" << endl; return 0;
		}
		else {
			string key = text + filename + (char)size + (char)color.r + (char)color.b + (char)color.g;

			if (text_list[key] == nullptr)
				text_list[key] = Graphics::Instance()->CreateTextTexture(font, text, color);

			return text_list[key];
		}
	}

	Mix_Music* AssetsManager::GetMusic(string filename)
	{
		string fullpath = SDL_GetBasePath();
		fullpath.append("assets/" + filename);

		if (music_list[fullpath] == nullptr)
		{
			music_list[fullpath] = Mix_LoadMUS(fullpath.c_str());
			if (music_list[fullpath] == NULL)
			{
				printf("Music loading Error : file-%s Error-%s", filename.c_str(), Mix_GetError());
			}
		}
		return music_list[fullpath];
	}

	Mix_Chunk * AssetsManager::GetSFX(string filename)
	{
		string fullpath = SDL_GetBasePath();
		fullpath.append("assets'\'" + filename);

		if (sfx_sound_list[fullpath] == nullptr)
		{
			sfx_sound_list[fullpath] = Mix_LoadWAV(fullpath.c_str());
			if (sfx_sound_list[fullpath] == NULL)
			{
				printf("SFX loading Error : file-%s Error-%s", filename.c_str(), Mix_GetError());
			}
		}
		return sfx_sound_list[fullpath];
	}
}
#include "PlayScreen.h"

using	namespace SDL;

PlayScreen::PlayScreen()
{
	timer = Timer::Instance();
	input = InputManager::Instance();

	level = new Level(0);
}


PlayScreen::~PlayScreen()
{
	timer = NULL;
	input = NULL;

	delete level;
	level = NULL;
}


void PlayScreen::Update()
{
	level->Update();
}

void PlayScreen::Render()
{
	level->Render();
}
#include "Scoreboard.h"



Scoreboard::Scoreboard()
{
	Score(0);
}


Scoreboard::~Scoreboard()
{
	ClearBoard();
}


void Scoreboard::ClearBoard()
{
	for (int i = 0; i < score.size(); i++)
	{
		delete score[i];
		score[i] = NULL;
	}
	score.clear();
}

void Scoreboard::Score(int scor)
{
	ClearBoard();

	if (scor == 0) {
		for (int i = 0; i < 2; i++)
		{
			score.push_back(new Texture("0", "Magic_Dreams.ttf", 32, { 230,230,230 }));
			score[i]->Parent(this);
			score[i]->Pos(Vector2(-32.0f*i, 0.0f));
		}
	}
	else
	{
		string str = to_string(scor);
		int lastIndex = str.length() - 1;

		for (int i = 0; i <= lastIndex; i++)
		{
			score.push_back(new Texture(str.substr(i, 1), "Magic_Dreams.ttf", 32, { 230,230,230 }));
			score[i]->Parent(this);
			score[i]->Pos(Vector2(-32.0f*(lastIndex - i), 0.0f));
		}
	}
}

void Scoreboard::Render()
{
	for (int i = 0; i < score.size(); i++)
	{
		score[i]->Render();
	}
}
#pragma once
#ifndef _BACKGROUNDLAYER_H
#define _BACKGROUNDLAYER_H

#include "BackgroundTexture.h"


class BackgroundLayer
{
private:
	static const int LAYER = 3;
	BackgroundTexture* backTex;
public:

	BackgroundLayer();
	~BackgroundLayer();

	void Update();
	void Render();
};

#endif // !_BACKGROUND_H
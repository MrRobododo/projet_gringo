#pragma once
#ifndef _STARSCREEN_H
#define _STARSCREEN_H

#include "AnimatedTexture.h"
#include "InputManager.h"
#include "Scoreboard.h"

using namespace SDL;
class StartScreen : public GameObject
{
private:

	Timer * mTimer;
	InputManager * input;
	Scoreboard *playerOneScore;
	Scoreboard *playerTwoScore;
	Scoreboard * topHighScore;


	// Top bar object -> partir de �a pour cr�e un hud
	GameObject * topBar;
	Texture * playerOne;
	Texture * playerTwo;
	Texture * highScore;

	//Play mode entities
	GameObject *playModes;
	Texture* onePlayerMode;
	Texture* twoPlayerMode;
	Texture * cursor;
	Vector2 cursorStartPos;
	Vector2 cursorOffset;
	int selectedMode;

	//bottom bar
	GameObject * botBar;
	Texture * entreprise;
	Texture * dates;
	Texture * rightsUser;

	// LOGO
	Texture * logo;
	AnimatedTexture* animatedLogo;

	//Screen Animation
	Vector2 animationStartPos;
	Vector2 animationEndPos;
	float animationTotalTime;
	float animationTimer;
	bool animationDonne;


public:
	StartScreen();
	~StartScreen();

	void ResetAnimation();

	int SelectedMode();

	void ChangeSelectedMode(int change);

	void Update();
	void Render();
};

#endif // !_STARSCREEN_H
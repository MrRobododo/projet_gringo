#pragma once
#ifndef BASE_H
#define BASE_H

#include <vector>
#include <map>
#include "SDL.h"

using namespace std;

enum  TILE {

	// 0 � 8
	left_top_corner,
	top_tile,
	right_top_corner,
	left_corner,
	tile,
	right_corner,
	left_bottom_corner,
	bottom_tile,
	right_bottom_corner,
	top_poll,
	middle_poll,
	bottom_poll,

	// block volant
	right_block_tile,
	block_tile,
	left_block_tile,

	// poteau
	simple_block,


	left_top_inside,
	right_top_inside,
	left_bottom_inside,
	right_bottom_inside,

	//slide_slode_right,
	//slide_slope_left,



};

enum  EVENT
{
	enemie = 20,
	drapeau = 21,

	evenement = 22,
	pieces = 23,

	moving_ground = 24,
};
enum
{
	/****ground id  [0,10]**/
	vide,
	ground,
	ground2,
	monster,
	invisible,
	event1,
	event2,
	event3,
	win,
	dead,
	bullet,


	//Pour le moment  la vector_map de map utilise des int en entre, mais si on l'aplemente avec des character on peut aller loin 
	// monster id [ 'a', 'z"]
	monster2 = 'b',
	invisible_monster = 'c',

	// event id [ 'A' , 'Z']


	//On peut aussi faire une refonte avec a chaque emplacement, un ID et TYPE
	/* ce qui nous donnerai une map 4,3
	0:0 0:0 0:0 0:0
	0:0 0:0 0:0 0:0
	1:0 1:0 1:0 1:0

	Je prefere l'idee d'une map de char mais �a demande un peu plus de boulot, a mon avis*/
};

//class Sound_Manager
//{
//public:
//	static Sound_Manager& Instance();
//private:
//	Sound_Manager & operator= (const Sound_Manager&) {}
//	Sound_Manager(const Sound_Manager&) {}
//
//	static Sound_Manager m_instance;
//
//	Sound_Manager();
//	~Sound_Manager();
//};
//

class baseclass {

protected:

	SDL_Texture * load_img(const char*, SDL_Renderer*);
	//void init_background(SDL_Window **window, SDL_Renderer **, int, int);
	//SDL_Texture * load_img(const string&name, SDL_Renderer*);
public:
	

};




class Renderer
{
public:
	Renderer();
	~Renderer() {

	}

	void start();
	void update(float deltatime);
};


#endif

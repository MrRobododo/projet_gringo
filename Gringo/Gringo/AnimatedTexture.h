#pragma once
#ifndef _ANIMATEDTEXTURE_H
#define _ANIMATEDTEXTURE_H

#include "Timer.h"
#include "Texture.h"

namespace SDL {
	class AnimatedTexture :public Texture
	{
	public:
		enum WRAP_MODE { once = 0, loop = 1 };

		enum ANIM_DIR { horizontal = 0, vertical = 1 };

	private:
		Timer * mTimer;

		int startX;
		int startY;

		float animationTimer;
		float animationSpeed;
		float timePerFrame;

		int frameCount;

		WRAP_MODE wrapMode;
		ANIM_DIR animationDirection;

		bool animationDonne;

	public:
		AnimatedTexture(string filenme, int x, int y, int w, int h, int frameCount, float animationSpeed, ANIM_DIR animationDir);
		~AnimatedTexture();

		void WrapMode(WRAP_MODE mode);
		void Update();


	};
}
#endif // !_ANIMATEDTEXTURE_H
#pragma once
#ifndef _TIMER_H
#define _TIMER_H
#include "SDL.h"

namespace SDL {
	class Timer
	{
	private:
		static Timer * instance;
		unsigned int startTicks;
		unsigned int elapsedTicks;
		float deltaTime;
		float timeScale;

	public:
		static Timer * Instance();
		static void Release();

		void Reset();
		float DeltaTimer();

		void TimeScale(float t);
		float TimerScale();

		void Update();

	private:
		Timer();
		~Timer();
	};

}
#endif
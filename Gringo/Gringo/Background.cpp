#include "Background.h"

Background* Background::instance = NULL;

Background* Background::Instance()
{
	if (instance == NULL)
		instance = new Background();

	return instance;
}

void Background::Released()
{
	delete instance;
	instance = NULL;
}


Background::Background()
{
	layers= new BackgroundLayer();

}

Background::~Background()
{
	delete layers;
	layers = NULL;
}


void Background::Update()
{
	layers->Update();
}

void Background::Render()
{
	//for (int i = 0; i < LAYER_COUNT; i++)
	//{
		layers->Render();
//	}
}
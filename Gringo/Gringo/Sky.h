#pragma once
#ifndef _SKY_H
#define _SKY_H
//#include "Cloud.h"
#include "Timer.h"
#include "Texture.h"

class Sky
{
private:
	static const int CLOUD_COUNT = 50;;
	//Cloud * clooud[CLOUD_COINT];


public:
	Sky();
	~Sky();

	void Update();
	void Render();
};

#endif // !_SKY_H
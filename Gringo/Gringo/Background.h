#pragma once
#ifndef _BACKGROUND_H
#define _BACKGROUND_H

#include <iostream>
#include  "BackgroundLayer.h"

class Background
{
private:
	static Background * instance;
	static const int LAYER_COUNT = 3;

	BackgroundLayer * layers;


public:
	static Background* Instance();
	static void Released();


	void Update();
	void Render();

private:
	Background();
	~Background();
};

#endif // !_BACKGROUND_H
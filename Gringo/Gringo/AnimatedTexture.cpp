#include "AnimatedTexture.h"

namespace SDL {
	AnimatedTexture::AnimatedTexture(string filename, int x, int y, int w, int h, int framCount, float animSpeed, ANIM_DIR animDir)
		:Texture(filename, x, y, w, h)
	{
		mTimer = Timer::Instance();

		//Where is the first frame
		startX = x;
		startY = y;

		frameCount = framCount;
		animationSpeed = animSpeed;
		timePerFrame = animationSpeed / frameCount;
		animationTimer = 0.0f;

		animationDirection = animDir;
		animationDonne = false;

		wrapMode = loop;
	}

	AnimatedTexture::~AnimatedTexture()
	{

	}

	void AnimatedTexture::WrapMode(WRAP_MODE mode)
	{
		wrapMode = mode;
	}

	void AnimatedTexture::Update()
	{
		if (!animationDonne)
		{
			animationTimer += mTimer->DeltaTimer();

			if (animationTimer >= animationSpeed)
			{
				if (wrapMode == loop)
				{
					animationTimer -= animationSpeed;
				}
				else {
					animationDonne = true;
					animationTimer = animationSpeed - timePerFrame;
				}
			}
			if (animationDirection == horizontal)
			{
				clipRect.x = startX + (int)(animationTimer / timePerFrame) * width;
			}
			else
			{
				clipRect.y = startY + (int)(animationTimer / timePerFrame) * height;
			}
		}
	}


}
#pragma once
#ifndef _LEVEL_H
#define _LEVEL_H


#include "BackgroundTexture.h"
#include "InputManager.h"
#include "LoadMap.h"
#include "Player.h"

class Level
{
	Timer * timer;
	SDL_Rect camera;
	BackgroundTexture * background;

	LoadMap * map1;

	Player * player1;

	int stages;
	bool stageStarted;
	 
	void Collision(vector<Texture*>tiles_list);

	void MoveToWorld(Texture *, float dx, float dy );

public:
	Level(int stage);
	~Level();

	void Scroll();

	void Update();
	void Render();
};

#endif // !_LEVEL_H
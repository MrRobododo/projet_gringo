#include "BackgroundTexture.h"



using namespace SDL;

bool BackgroundTexture::bScrool = false;



BackgroundTexture::BackgroundTexture(SDL_Rect camera) :Texture("background.bmp")
{
	timer = Timer::Instance();
	printf("CREATE BACKGROUND");
	pos.x = camera.x;
	pos.y =  Graphics::Instance()->SCREEN_HEIGHT /2;
	clipped = false;

	scrollingOffset = 0;

	if (height < camera.h)
	{
		Scale(Vector2(camera.h/height, camera.h / height));
	}
}


BackgroundTexture::~BackgroundTexture()
{
	timer = NULL;
}

void BackgroundTexture::ScrollSky()
{
	if (scrollingOffset < -width)
	{
		scrollingOffset = 0;
	}

}

void BackgroundTexture::Update()
{

}

void BackgroundTexture::Render(SDL_Rect camera, LoadMap* map)
{
	Texture::Render();
}
#pragma once
#ifndef _TEXTURE_H
#define _TEXTURE_H

#include "GameObject.h"
#include "AssetsManager.h"

using namespace std;
namespace SDL {

	class Texture : public GameObject
	{

	private : 
		static const int TILE_W_PXL = 16;
		static const int TILE_H_PXL = 16;
	protected:
		SDL_Texture * tex;
		Graphics * graphics;
		int type;


		bool clipped;
		SDL_Rect clipRect;
		SDL_Rect renderRect;

	public:
		int reference;
		int width;
		int height;
		SDL_Rect rect;

	public:
		Texture(string filename);
		Texture(string filename, int x, int y, int w, int h);
		Texture(string filename, int x, int y, int w, int h, bool cliped);
		Texture(string filename, int posX, int posY,int w, int h, int event, int id, int number);
		Texture(string text, string fontpath, int size, SDL_Color color);
		~Texture();

		int Type() { return type; };
		string path;

		virtual void Render();

		virtual void Render(SDL_Rect*camera);

		virtual void Render(SDL_Rect*camera, SDL_RendererFlip flip);
	};
}
#endif // !_TEXTURE_H
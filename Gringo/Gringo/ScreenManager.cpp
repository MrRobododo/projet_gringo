#include "ScreenManager.h"

ScreenManager * ScreenManager::instance = NULL;

ScreenManager*ScreenManager::Instance()
{
	if (instance == NULL)
		instance = new ScreenManager();

	return instance;
}


void ScreenManager::Release()
{
	delete instance;
	instance = NULL;
}


ScreenManager::ScreenManager()
{
	input = InputManager::Instance();
	/*background = Background::Instance();*/
	//startscren = new StartScreen();
	playscren = new PlayScreen();

	currentScreen = play;

}

ScreenManager::~ScreenManager()
{
	input = NULL;
	/*Background::Released();*/
		/*background->Render();*/

	//delete startscren;
	//startscren = NULL;

	delete playscren;
	playscren = NULL;
}


void ScreenManager::Update()
{
	/*background->Update();*/

	switch (currentScreen)
	{
	case ScreenManager::start:
		break;
		//startscren->Update();
		if (input->KeyPressed(SDL_SCANCODE_RETURN))
		{
			currentScreen = play;

			//startscren->ResetAnimation();
		}
		break;
	case ScreenManager::play:
		playscren->Update();
		if (input->KeyPressed(SDL_SCANCODE_RETURN))
		{
			currentScreen = start;
			printf("current screen start");

		}
		break;
	default:
		break;
	}


}

void ScreenManager::Render()
{


	switch (currentScreen)
	{
	case ScreenManager::start:
		//startscren->Render();
		
		break;
	case ScreenManager::play:
	
		playscren->Render();
		break;
	default:
		break;
	}

	/*background->Render();*/
}
#pragma once
#ifndef _GRAPHICS_H
#define _GRAPHICS_H

#include <iostream>
#include <string>
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

using namespace std;
namespace SDL {
	class Graphics
	{
	public:

		static const int SCREEN_WIDTH = 900;
		static const int SCREEN_HEIGHT = 600;

		const char *WINDOW_TITLE = "Projet_gringo";

	private:
		static Graphics * instance;
		static bool init;

		SDL_Window * window;
		/*SDL_Surface *mBackBuffer;*/
		SDL_Renderer * renderer;

	public:
		static Graphics *Instance();
		static void Release();
		static bool Initialized();

		SDL_Texture *LoadTexture(string path);
		void DrawTexture(SDL_Texture* tex, SDL_Rect * clip = NULL, SDL_Rect* render = NULL, float angle = 0.0f, SDL_RendererFlip flip = SDL_FLIP_NONE);

		SDL_Texture * CreateTextTexture(TTF_Font*font, string text, SDL_Color color);

		void ClearBackBuffer();

		void Render();

	private:
		Graphics();		// constructeur en private -> singleton
		~Graphics();

		bool Init();
	};
}
#endif 
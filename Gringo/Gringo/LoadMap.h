#ifndef _LOADMAP_H
#define _LOADMAP_H


#include <vector>
#include <fstream>
#include "Texture.h"
#include "Player.h"

using namespace std;
using namespace SDL;

typedef struct {
	int LARGEUR_TILE, HAUTEUR_TILE;
	int nbtiles;
	int **schema;
	int nbtiles_largeur_monde, nbtiles_hauteur_monde;
	int xscroll, yscroll;
	int largeur_fenetre, hauteur_fenetre;
} Map;



class LoadMap
{

private:

	enum TILE
	{
		left_top_corner,
		top_tile,
		right_top_corner,
		left_corner,
		tile,
		right_corner,
		left_bottom_corner,
		bottom_tile,
		right_bottom_corner,


		top_poll,
		middle_poll,
		bottom_poll,

		// block volant
		right_block_tile,
		block_tile,
		left_block_tile,

		// poteau
		simple_block,


		left_top_inside,
		right_top_inside,
		left_bottom_inside,
		right_bottom_inside,
	};

	enum  EVENT
	{
		enemie = 20,
		drapeau = 21,

		evenement = 22,
		pieces = 23,

		moving_ground = 24,
	};

	enum
	{
		/****ground id  [0,10]**/
		vide,
		ground,
		wall,
		monster,
		invisible,
		grid
		//event1,
		//event2,
		//event3,
		//win,
		//dead,
		//bullet,
	};


public:
	int** bg;

	int LEVEL_WIDTH;
	int LEVEL_HEIGHT;

	int MAP_WIDTH;
	int Width() { return MAP_WIDTH; };
	int MAP_HEIGHT;
	int Height() { return MAP_HEIGHT; };


	const int TILE_WIDTH = 50;
	const int TILE_HEIGHT = 50;

	vector<vector<int>> vector_map;

	vector< Texture*> tile_list;
private:

	SDL_Rect  camera;
	int mapX, mapY;


	int xscroll, yscroll;


	
	bool nc;

private:

	int ** create_bg_array(int, int);
	void destroy_bg_array(int **, int);

	void add_object(int PosX, int PosY, int event, int id, int nb)
	{
		Texture* texture = new Texture("tileset.bmp",PosX, PosY, TILE_WIDTH, TILE_HEIGHT,event, id, nb);
		tile_list.push_back(texture);
	//	bg[PosX][PosY] = event;
			/*if (event == ground || event == ground2) {
				for (int i = texture->pos.x; i < texture->pos.x + TILE_SIZE_X; i++) {
					for (int j = texture->pos.y; j < texture->pos.y + TILE_SIZE_Y; j++) {
						bg[i][j] = event;
					}
				}
			}*/
	
			//	//	entity *new_entity = new entity(bg, PosX, PosY, WIDTH, HEIGHT, start, end, event, id, direction, mapRenderer);
			//	//	active_object_list.insert(st_ob(name, *new_entity));
			//
			//	//}
	}

	void add_object(int PosX, int PosY)
	{
		cout << "__ADD Ground_____" << endl;
		Texture* texture = new Texture("grid.bmp", PosX, PosY, TILE_WIDTH, TILE_HEIGHT,true);
		tile_list.push_back(texture);
		/*bg[PosX][PosY] = grid;*/
	}


public:
	LoadMap(SDL_Rect);
	~LoadMap();

	void Scroll(int x, int y);

	void Collission(Player*);

	void load_vector_Map(const char *filename);
	void load_Map(vector<vector<int>>);

	void Update();
	void Move();
	void Render();
};



#endif 
//#pragma once
//#include <iostream>
//#include <fstream>
//#include <vector>
//#include <map >
//
//#include "base.h"
//
//#include "entity.h"
//#include "player.h"
//
//
//
//using namespace std;
//
//typedef pair<string, entity> st_ob;
//typedef map<string, entity> m_st_ob;
//
//
//static int nb;
//
//class Map 
//{
//
//public:
//
//	// D�claration des constante de map
//	/*static const int TILE_SIZE_W = 50;
//	static const int TILE_SIZE_H = 50;*/
//
//	int xscroll, yscroll;
//
//	int MAP_WIDTH;
//	int MAP_HEIGHT;
//
//	int LEVEL_WIDTH;
//	int LEVEL_HEIGHT;
//
//	int** bg;
//	SDL_Renderer* mapRenderer;
//	//definition des liste d'objets generer par la map
//
//	vector <entity*> entity_list;
//	vector <entity*> moving_entity_list;
//	vector <Bullet*> bullet_list;
//	vector <Monster*> monster_list;
//	m_st_ob active_object_list;
//	bool nc;
//
//	SDL_Texture *bul, *grd;
//	vector<vector<int>> vector_map;
//
//	int ** create_bg_array(int, int);
//	void destroy_bg_array(int **, int);
//
//	const char* getImg_map(int id)
//	{
//		const char *c;
//
//		switch (id)
//		{
//		case 0:
//			c = "assets/map.map";
//			return c;
//			break;
//
//		case  1:
//
//			break;
//
//		default:
//			break;
//		}
//	}
//
//	//Constructeur | Destructeur
//	//map();
//	Map(SDL_Renderer*, int level); // A d�finir
//	~Map();
//
//	vector<vector<int>> getMap_Vector() { return this->vector_map; };
//
//	// D�finition des m�thodes 
//	void load_vector_Map(const char *filename);
//	void load_Map(vector<vector<int>>, SDL_Texture*cc, SDL_Texture*bull);
//	void show_Map(SDL_Renderer * render, vector<vector<int>>  vector_map, int **bg, SDL_Rect camera);
//
//
//	void move(int** bg, player* player);
//
//	void render(SDL_Rect* camera, SDL_Renderer*);
//
//	int check_event(player* person);
//	void clean();
//
//	void render2(SDL_Rect* cam, SDL_Renderer *render)
//	{
//		for (int i = 0; i < bullet_list.size(); i++)
//		{
//			bullet_list[i]->show(render, cam);
//		}
//	}
//
//	// different du handle_event du player
//
//
//	void add_object(int PosX, int PosY, int WIDTH, int HEIGHT, int event, int id, SDL_Texture* tex)
//	{
//		//cout << "__ADD Ground_____" << endl;
//		entity* new_entity = new entity(bg, PosX, PosY, WIDTH, HEIGHT, event, id, tex, mapRenderer);
//		entity_list.push_back(new_entity);
//	}
//
//	// add moving object
//	void add_object(int PosX, int PosY, int WIDTH, int HEIGHT, int start, int end, int event, int direction, int id)
//	{
//		/*cout << "_______" << endl;*/
//		entity* new_entity_moving = new entity(bg, PosX, PosY, WIDTH, HEIGHT, start, end, event, id, direction, mapRenderer);
//		moving_entity_list.push_back(new_entity_moving);
//	}
//
//
//	//// add active object
//	//void add_object(string name, int PosX, int PosY, int WIDTH, int HEIGHT, int start, int end, int event, int id, int direction)
//	//{
//	//	cout << "_______" << endl;
//	//	entity *new_entity = new entity(bg, PosX, PosY, WIDTH, HEIGHT, start, end, event, id, direction, mapRenderer);
//	//	active_object_list.insert(st_ob(name, *new_entity));
//
//	//}
//	void add_monster(int PosX, int PosY, int W, int H, int e, int id,SDL_Texture * monst)
//	{
//		cout << "add monster : " << nb << "||" << endl;
//		Monster * new_monster = new Monster(bg, PosX, PosY, W, H, e, id, mapRenderer, monst);
//		monster_list.push_back(new_monster);
//	}
//
//
//	void add_bullet(int ** bg, int PosX, int PosY, int event, char direction, int id, SDL_Renderer * renderer, SDL_Texture* bul)
//	{
//		cout << "___bullet____" << endl;
//		int dir;
//		int xvel;
//		if (direction = 'r'){
//			xvel = 5;
//		}
//		else {
//			xvel = -5;
//		}
//		Bullet* new_bullet = new Bullet(bg, PosX, PosY, xvel, 0, event, id, renderer, bul);
//		bullet_list.push_back(new_bullet);
//	}
//
//	void handel_event_keyPressed(SDL_Event e, player*player, SDL_Renderer *renderer,SDL_Texture * bul)
//	{
//		player->handle_event(bg, e, renderer);
//		if (e.type == SDL_KEYDOWN && e.key.repeat == 0)
//		{
//			switch (e.key.keysym.sym)
//			{
//
//			case SDLK_f:
//				if (player->getDirection() == 'r')
//				{
//					add_bullet(bg, player->getRect()->x - (player->getRect()->w / 2), player->getRect()->y + (player->getRect()->h / 2), bullet, player->getDirection(), 0,renderer, bul);
//				}
//				else
//				{
//					add_bullet(bg, player->getRect()->x - (player->getRect()->w / 2), player->getRect()->y + (player->getRect()->h / 2), bullet, player->getDirection(), 0,renderer, bul);
//					////bullets.push_back(new bullet(renderer,
//					////player1->getRect()->x - (player1->getRect()->w) / 2,
//					////player1->getRect()->y + 15, -5, 0));
//				}
//				break;
//			}
//		}
//	}
//
//
//	void handel_event(m_st_ob& list, string name, int event, int** bg, SDL_Renderer* gRenderer)
//	{
//		m_st_ob::iterator itr;
//		itr = list.find(name);
//
//		switch (event)
//		{
//		case bullet:
//			for (int i = 0; i < bullet_list.size(); i++)
//			{
//				bullet_list[i]->move(bg);
//			}
//			break;
//		case invisible:
//			itr->second.change_type(ground, bg);
//			break;
//
//		case event1:
//			itr->second.change_type(ground, bg);
//			/*itr->second.change_img(gRenderer, "source/picture/background/stand.png");*/
//			/*itr = list.find("event1-a");
//			itr->second.change_type(ground, bg);*/
//			break;
//
//		case event2:
//			//itr->second.change_type(empty, bg);
//			////itr->second.change_img(gRenderer, "source/picture/background/event2-a.png");
//			//itr = list.find("event2-a");
//			//itr->second.change_direction(U_and_D);
//			break;
//
//		case event3:
//			//itr->second.change_type(empty, bg);
//			////itr->second.change_img(gRenderer, "");
//			//itr = list.find("event3-a");
//			//itr->second.change_type(win, bg);
//			break;
//
//			//case event4:
//				//itr->second.change_type(empty, bg);
//				//itr = list.find("event4-a");
//				//itr->second.change_type(ground, bg);
//				////itr->second.change_img(gRenderer, "source/picture/background/event4.png");
//				//itr = list.find("event4-b");
//				//itr->second.change_direction(Monster);
//			break;
//
//		case win:
//			break;
//
//		case monster:
//			break;
//
//		case dead:
//			break;
//
//		default:
//			/*cout<< "Unknown event: "<<  itr->first << " " << itr->second.get_event() << endl;*/
//			break;
//		}
//
//		//return;
//	}
//
//};
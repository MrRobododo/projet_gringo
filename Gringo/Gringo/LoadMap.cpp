#include "LoadMap.h"


LoadMap::LoadMap(SDL_Rect cam)
{
	bg = create_bg_array(LEVEL_WIDTH, LEVEL_HEIGHT);
	load_vector_Map("assets/map.map");
	camera = cam;

	mapX = mapY = 0;


}


LoadMap::~LoadMap()
{
}

int**LoadMap::create_bg_array(int LEVEL_WIDTH, int LEVEL_HEIGHT) {
	int** background_array = new int*[LEVEL_WIDTH];
	for (int i = 0; i < LEVEL_WIDTH; i++) {
		background_array[i] = new int[LEVEL_HEIGHT]();
	}
	return background_array;
}

void LoadMap::destroy_bg_array(int** bg, int LEVEL_WIDTH) {
	for (int i = 0; i < LEVEL_WIDTH; i++) {
		delete[] bg[i];
	}
	delete[] bg;
}


void LoadMap::Scroll(int x, int y)
{
	for (int i = 0; i < tile_list.size(); i++)
	{
		tile_list[i]->pos.x = tile_list[i]->pos.x + x;
		tile_list[i]->pos.y = tile_list[i]->pos.y + y;
	}
}

void LoadMap::load_vector_Map(const char *filename)
{
	printf("loading the map\n");
	// On importe .map dans in
	std::ifstream in(filename);

	if (!in.is_open()) {
		std::cout << "problem with loading the file " << SDL_GetError();
		return;
	}

	//Il faut recuperer ces deux valeurs sinon le chargement de la map est d�call�
	int width, height;
	in >> width;
	in >> height;
	MAP_WIDTH = width;
	MAP_HEIGHT = height;

	LEVEL_WIDTH = width * TILE_WIDTH;//  TILE_SIZE;
	LEVEL_HEIGHT = height * TILE_HEIGHT;//TILE_SIZE;

	// Current est la valeur de l'�lement a la position {i,j} dans le cas ci-dessus �a peut �tre 0,1 ou 2  
	int current;

	for (int i = 0; i < MAP_HEIGHT; i++)
	{
		std::vector<int> vec;	// Initialisation du vecteur ligne qui va stocker toutes les valeurs de la matrice .map
		for (int j = 0; j < MAP_WIDTH; j++)
		{
			if (in.eof()) {
				std::cout << "problem with loading the map " << SDL_GetError();
				return;
			}
			in >> current;// On stocke la valeur de in dans current
			if (current >= -1 && current <= 100) {					// current prends toutes les valeurs de 0 � 9
				vec.push_back(current);	// Si il y a une valeur on la push dans le vecteur
			}
			else {
				vec.push_back(-1);		// si non on place un zero en derni�re position
			}
		}
		vector_map.push_back(vec);	// On push tout les vecteur pour former un grand vecteur .map
	}
	in.close(); 	// On ferme le fichier
	load_Map(vector_map);
}



void LoadMap::load_Map(vector<vector<int>>  vector_map)
{
	int nb = 0;
	for (int i = 0; i < vector_map.size(); i++)
	{
		for (int j = 0; j < vector_map[0].size(); j++)
		{
			if (vector_map[i][j] != -1)
			{
				int curent = vector_map[i][j];
				//SDL_Rect destRect_map = {	//quand a dest rect est l'endroit ou il est affich�
				//  j * TILE_SIZE_X, //TILE_SIZE,
				//  i * TILE_SIZE_Y, //TILE_SIZe,
				//  TILE_SIZE_X, //TILE_SIZE,
				//  TILE_SIZE_Y,//TILE_SIZE 
				//};

				TILE checkTile = static_cast<TILE>(curent);
				if (checkTile >= left_top_corner && checkTile <= right_bottom_inside) {}
				{
					//if (checkTile == top_tile || checkTile == tile || checkTile == left_bottom_corner || checkTile == bottom_tile || checkTile == right_top_corner || checkTile == left_bottom_inside || checkTile == right_bottom_inside)
					//{
						add_object(j*TILE_WIDTH, i*TILE_HEIGHT, ground, curent,nb );
					
					//if (checkTile == left_corner || checkTile == right_corner || checkTile == top_poll || checkTile == middle_poll || checkTile == bottom_tile)
					//{
						/*add_object(j*TILE_WIDTH, i*TILE_HEIGHT, wall, curent);*/
					
				}
				nb++;
			}
			else
			{
				add_object(j*TILE_WIDTH, i*TILE_HEIGHT);
			}
		}
	}
}


void  LoadMap::Move()
{
}


void LoadMap::Render()
{
	int x, y, n, x1, x2, y1, y2, mx, my;

	x1 = camera.x%TILE_WIDTH*-1;
	x2 = x1 + Graphics::Instance()->SCREEN_WIDTH*TILE_WIDTH + (x1 == 0 ? 0 : TILE_WIDTH);

	y1 = camera.y%TILE_HEIGHT*-1;
	y2 = y1 + Graphics::Instance()->SCREEN_HEIGHT*TILE_HEIGHT + (y1 == 0 ? 0 : TILE_HEIGHT);

	mx = camera.x / TILE_WIDTH;
	my = camera.y / TILE_HEIGHT;



	for (int i = 0; i < tile_list.size(); i++)
	{
		if (tile_list[i]->pos.x >= mapX - TILE_WIDTH ||
			tile_list[i]->pos.y >= mapY - TILE_HEIGHT ||
			tile_list[i]->pos.x < mapX + MAP_WIDTH + TILE_WIDTH ||
			tile_list[i]->pos.y <= mapY + MAP_HEIGHT + TILE_HEIGHT)
		{
			tile_list[i]->Render(&camera);
		}
		
	}
}

//// Constructeur
////map::map()
////{
////}
//
//
////A d�finir / pr�vision pour plus tard
//Map::Map(SDL_Renderer *renderer, int level)
//{
//	load_vector_Map(getImg_map(level));
//	bg = create_bg_array(LEVEL_WIDTH, LEVEL_HEIGHT);
//	mapRenderer = renderer;
//}
//
//// Destructeur
//Map::~Map()
//{
//}
//
//int** Map::create_bg_array(int LEVEL_WIDTH, int LEVEL_HEIGHT) {
//	int** background_array = new int*[LEVEL_WIDTH];
//	for (int i = 0; i < LEVEL_WIDTH; i++) {
//		background_array[i] = new int[LEVEL_HEIGHT]();
//	}
//	return background_array;
//}
//
//void Map::destroy_bg_array(int** bg, int LEVEL_WIDTH) {
//	for (int i = 0; i < LEVEL_WIDTH; i++) {
//		delete[] bg[i];
//	}
//	delete[] bg;
//}
//
//void Map::load_vector_Map(const char *filename)
//{
//	// On importe .map dans in
//	std::ifstream in(filename);
//
//	if (!in.is_open()) {
//		std::cout << "problem with loading the file " << SDL_GetError();
//		return;
//	}
//
//	//Il faut recuperer ces deux valeurs sinon le chargement de la map est d�call�
//	int width, height;
//	in >> width;
//	in >> height;
//	MAP_WIDTH = width;
//	MAP_HEIGHT = height;
//
//	LEVEL_WIDTH = width * 50;//  TILE_SIZE;
//	LEVEL_HEIGHT = height * 50;//TILE_SIZE;
//
//	// Current est la valeur de l'�lement a la position {i,j} dans le cas ci-dessus �a peut �tre 0,1 ou 2  
//	int current;
//
//	for (int i = 0; i < MAP_HEIGHT; i++)
//	{
//		std::vector<int> vec;	// Initialisation du vecteur ligne qui va stocker toutes les valeurs de la matrice .map
//		for (int j = 0; j < MAP_WIDTH; j++)
//		{
//			if (in.eof())			{
//				std::cout << "problem with loading the map " << SDL_GetError();
//				return;	
//			}			
//			in >> current;// On stocke la valeur de in dans current
//			if (current >= -1 && current <= 100){					// current prends toutes les valeurs de 0 � 9
//				vec.push_back(current);	// Si il y a une valeur on la push dans le vecteur
//			}else			{
//				vec.push_back(-1);		// si non on place un zero en derni�re position
//			}
//		}	
//		vector_map.push_back(vec);	// On push tout les vecteur pour former un grand vecteur .map
//	}
//	in.close(); 	// On ferme le fichier
//}
//
//
//
//void Map::load_Map(vector<vector<int>>  vector_map,SDL_Texture *blck, SDL_Texture *monter)
//{
//	for (int i = 0; i < vector_map.size(); i++)
//	{
//		for (int j = 0; j < vector_map[0].size(); j++)
//		{
//			if (vector_map[i][j] != -1)
//			{
//				int curent = vector_map[i][j];
//				SDL_Rect destRect_map = {	//quand a dest rect est l'endroit ou il est affich�
//				  j*50, //TILE_SIZE,
//				  i*50, //TILE_SIZe,
//				  50, //TILE_SIZE,
//				  50,//TILE_SIZE 
//				};
//
//				TILE checkTile = static_cast< TILE >(curent);
//				if (checkTile >= left_top_corner && checkTile <= right_bottom_inside)
//				{
//					add_object(destRect_map.x, destRect_map.y, destRect_map.w, destRect_map.h, ground, curent, blck);
//				}
//				EVENT checkEvent = static_cast<EVENT>(curent);
//				if (checkEvent >= enemie && checkEvent <= pieces) {
//					switch (curent)
//					{
//					case enemie:
//						add_monster(destRect_map.x, destRect_map.y, destRect_map.w, destRect_map.h, monster, curent, monter);
//
//						break;
//					case drapeau:
//						cout << "drapeau : " << curent;
//						//add_object("drapeau", destRect_map.x, destRect_map.y, destRect_map.w, destRect_map.h, 0, 0, win, curent, None);
//						break;
//					case evenement:
//						//cout << "event1 : " << curent;
//						//add_object( string ("event1 "+nb), destRect_map.x, destRect_map.y, destRect_map.w, destRect_map.h, 0, 0, event1, curent, None); 
//						//add_object(string("event1 " + nb+1), destRect_map.x+TILE_SIZE_W, destRect_map.y, destRect_map.w, destRect_map.h, 0, 0, event1, curent, None);
//
//						break;
//					case pieces:
//						/*cout << "pieces : " << curent;
//						add_object(destRect_map.x, destRect_map.y, destRect_map.w, destRect_map.h, ground2, curent);*/
//						break;
//					default:
//						break;
//					}				
//
//					if (curent == moving_ground)
//					{
//						/*add_object("moving_ground", destRect_map.x, destRect_map.y, destRect_map.w, destRect_map.h, 0, 0, ground, curent, None);*/
//					}
//				}					
//			}
//		}
//	}
//
//}
//
//void  Map::render(SDL_Rect* camera,SDL_Renderer *render)
//{
//	for (int i = 0; i < entity_list.size(); i++)
//	{
//		entity_list[i]->render(mapRenderer, camera);
//	}
//	for (int i = 0; i < moving_entity_list.size(); i++) {
//		moving_entity_list[i]->render(mapRenderer, camera);
//	}
//	for (int i = 0; i < bullet_list.size(); i++)
//	{
//		bullet_list[i]->show(render,camera);
//	}
//	for (int i = 0; i < monster_list.size(); i++)
//	{
//		monster_list[i]->render(mapRenderer, camera);
//	}
//
//
//	m_st_ob::iterator itr = active_object_list.begin();
//	for (itr; itr != active_object_list.end(); itr++)
//	{
//		itr->second.render(mapRenderer, camera);
//	}
//}
//
//void  Map::move(int** bg, player* player)
//{
//	for (int i = 0; i < moving_entity_list.size(); i++)
//	{
//		moving_entity_list[i]->move(bg, player);
//	}
//	m_st_ob::iterator itr = active_object_list.begin();
//
//	for (itr; itr != active_object_list.end(); itr++)
//	{
//		if (itr->second.get_event() == ground)
//			itr->second.move(bg, player);
//		/*else if (itr->second.get_event() == monster)
//			itr->second.move_monster(bg, player);*/
//	}
//
//	for (int i = 0; i < monster_list.size(); i++)
//	{
//		monster_list[i]->move_monster(bg, player);
//	}
//
//	//for (int i = 0; i < bullet_list.size(); i++)
//	//{
//	//	bullet_list[i]->move(bg);
//	//}
//}
//
//
//int Map::check_event(player* player) {
//	m_st_ob::iterator itr = active_object_list.begin();
//
//	for (itr; itr != active_object_list.end(); itr++)
//	{
//		int event = itr->second.check_collision(player);
//
//		if (event != vide && event != ground)
//		{
//			// Ce qui va permetre de gerer la liste des objet activable
//			 handel_event(active_object_list, itr->first, event, bg, mapRenderer);
//		}
//		if (event == monster || event == dead) {
//			return dead;
//		}
//		if (event == win) {
//			return win;
//		}
//	}
//	return vide;
//}
//
//void  Map::clean()
//{
//	for (int i = 0; i < entity_list.size(); i++)
//	{
//		entity_list[i]->destroy();
//	}
//	for (int i = 0; i < moving_entity_list.size(); i++)
//	{
//		moving_entity_list[i]->destroy();
//	}
//	m_st_ob::iterator itr = active_object_list.begin();
//	for (itr; itr != active_object_list.end(); itr++)
//	{
//		itr->second.destroy();
//	}
//	for (int i = 0; i < monster_list.size(); i++)
//	{
//		monster_list[i]->destroy();
//	}
//	for (int i = 0; i < bullet_list.size(); i++)
//	{
//		bullet_list[i]->destroy();
//	}
//}
//

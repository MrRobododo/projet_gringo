#include "Player.h"

#define SGN(X) (((X)==0)?(0):(((X)<0)?(-1):(1)))

using namespace SDL;

Player::Player()
{
	timer = Timer::Instance();
	input = InputManager::Instance();

	playerTex = new Texture("player.bmp", 0, 0, 50, 50);
	playerWalking = new AnimatedTexture("player.bmp", 0, 0, 50, 50, 8, 0.8f, AnimatedTexture::horizontal);
	playerTex->Parent(this);

	playerWalking->Parent(this);
	offset_X = 200;
	Pos(Vector2(offset_X, 300));
	
	bOnGround = false;
	bFalling = true;

	bCollisionRight = bCollisionLeft = false;

}

Player::~Player()
{
}

void Player::Set_camera(SDL_Rect *camera, Player *player, int LEVEL_WIDTH, int LEVEL_HEIGHT)
{
	camera->x = (pos.x + playerSize_W / 2) - camera->w / 2;
	camera->y = (pos.y + playerSize_H / 2) - camera->h / 2;
	if (camera->x < 0) {
		camera->x = 0;
	}
	if (camera->y < 0) {
		camera->y = 0;
	}
	//	if (camera->x > LEVEL_WIDTH - camera->w ) {
		//	camera->x = LEVEL_WIDTH - camera->w;
		//}
		//if (camera->y >  LEVEL_HEIGHT - camera->h) {
		//	camera->y = LEVEL_HEIGHT - camera->h;
		//}  
	if (camera->x + camera->w > LEVEL_WIDTH) {
		camera->x = LEVEL_WIDTH - camera->w;
	}
	if (camera->y + camera->h > LEVEL_HEIGHT) {
		camera->y = LEVEL_HEIGHT - camera->h;
	}
}
void Player::Update()
{
	//cout << "is jumping " << bJumping << endl;
	//cout << "is falling " << bFalling << endl;
	//cout << "on Ground " << bOnGround << endl;
	//cout << "bcollision right" << bCollisionRight << endl;
	//cout << "bcollision left" << bCollisionLeft << endl;
	cout << "xvel "<<xVelocity << endl;
	//cout << "bCollide " << bCollide << endl;



	if (input->KeyPressed(SDL_SCANCODE_RIGHT))
	{
		if (!bCollisionRight)
		{
			bMoving = true;
			bDirection = true;
			xVelocity += speedX;
		}

	}
	else if (input->KeyPressed(SDL_SCANCODE_LEFT))
	{
		if (!bCollisionLeft) {
			bMoving = true;;
			bDirection = false;
			xVelocity -= speedX;
		}
	}

	else if (input->KeyPressed(SDL_SCANCODE_UP) && bOnGround)
	{
		if (!bJumping)
		{
			cout << "jumping" << endl;
			bJumping = true;
			yVelocity = -speedY * 2;
		}
	}

	else if (input->KeyReleased(SDL_SCANCODE_RIGHT))
	{
		bMoving = false;
		if (xVelocity != 0)
			xVelocity -= speedX;
	}
	else if (input->KeyReleased(SDL_SCANCODE_LEFT))
	{
		bMoving = false;
		if (xVelocity != 0)
			xVelocity += speedX;
	}

	//else if (input->KeyReleased(SDL_SCANCODE_UP))
	//{
	//	if (yVelocity < -7.0f)
	//	{
	//		cout << "call" << endl;
	//		yVelocity = +speedY;
	//	}
	//}


	playerWalking->Update();
}



void Player::MoveToWordl(vector<Texture*> tile_list)
{
	SDL_Rect bot = { pos.x, pos.y+1, playerSize_W, playerSize_H + 1 };
	SDL_Rect left = { pos.x - 1, pos.y+1, playerSize_W/2,playerSize_H-1 };
	SDL_Rect right = { pos.x +(playerSize_W/2) , pos.y+1, (playerSize_W/2)+1, playerSize_H -1};
	SDL_Rect top = { pos.x , pos.y - 1, playerSize_W, playerSize_H-1 };



	/*	for (int i = 0; i < tile_list.size(); i++)
		{
			if (Collssion(left, tile_list[i]))
			{
				if (tile_list[i]->Type() == 1)
				{
					bCollisionLeft = true;
				}
				else
				{
					bCollisionLeft = false;
				}
				
			}
		}*/
	if (xVelocity !=0) {
		if (xVelocity > 0) {
			for (int i = 0; i < tile_list.size(); i++)
			{
				SDL_Rect tile = { tile_list[i]->pos.x, tile_list[i]->pos.y, tile_list[i]->width, tile_list[i]->height };
				if (SDL_HasIntersection(&right, &tile))
				{
					if (tile_list[i]->Type() == 1)
					{
						WALL_RIGHT = tile_list[i]->pos.x;
						WALL_LEFT = NULL;
						bCollisionRight = true;
						
					}
					else {
						bCollisionRight = false;
						//pos.x = pos.x + xVelocity;
					}
				}
			}
		}
		if (xVelocity < 0) {
			for (int i = 0; i < tile_list.size(); i++)
			{
				SDL_Rect tile = { tile_list[i]->pos.x, tile_list[i]->pos.y, tile_list[i]->width, tile_list[i]->height };
				if (SDL_HasIntersection(&left, &tile))
				{
					if (tile_list[i]->Type() == 1)
					{
						WALL_LEFT = tile_list[i]->pos.x + tile_list[i]->width;
						WALL_RIGHT = NULL;
						bCollisionLeft = true;
					}
					else {
						cout << "call ";
						bCollisionLeft = false;
						//pos.x = pos.x + xVelocity;
					}
				}
			}
		}
	}

	if (!bCollisionRight)
	{
		pos.x = pos.x + xVelocity;
	}
	if (bCollisionRight)
	{
		if (WALL_RIGHT !=NULL)	{
			pos.x = WALL_RIGHT - playerSize_W;
		}
	}

	if (bCollisionLeft)
	{
		if (WALL_LEFT != NULL) {
			pos.x = WALL_LEFT;
		}
	}

	//if (!bCollisionRight || !bCollisionLeft)
	//{
	//	pos.x = pos.x + xVelocity;
	//}


	for (int i = 0; i < tile_list.size(); i++)
	{
		if (Collssion(bot, tile_list[i]))
		{
			if (tile_list[i]->Type() == 1)
			{
				GROUND_Y = tile_list[i]->pos.y;
				
				bOnGround = true;
				bFalling = false;

			}
			else
			{
				bOnGround = false;
				bFalling = true;
			}
		}
	}

	if (bOnGround)
	{
		pos.y = GROUND_Y - playerSize_H;
	}

	if (bFalling && !bOnGround)
	{
		pos.y = pos.y + gravity;
	}


}


bool Player::Collssion(SDL_Rect a, Texture *b)
{
	if (a.x >= b->pos.x + b->width)
	{
		return false;
	}
	if (a.x + a.w < b->pos.x)
	{
		return false;
	}
	if (a.y >= b->pos.y + b->height)
	{
		return false;
	}
	if (a.y + playerSize_H  <= b->pos.y)
	{
		//printf(" collission");
		return false;
	}
	else
	{
		return true;
	}
}


//int Player::EssaieDeplacement(Texture* tileTex, int vx, int vy)
//{
//	SDL_Rect test;
//	test.x = pos.x;
//	test.y = pos.y;
//	test.x += vx;
//	test.y += vy;
//	test.w = playerSize_W;
//	test.h = playerSize_H;
//
//	if (Collssion(tileTex))
//	{
//		if (tileTex->Type() == 1)
//		{
//			pos.x = test.x;
//			pos.y = test.y;
//
//			return 1;
//		}
//	}
//	return 0;
//
//}

//void Player::Affine(Texture* tileTex, int x, int y)
//{
//	int i;
//	for (i = 0; abs(xVelocity); i++)
//	{
//		if (EssaieDeplacement(tileTex, SGN(x), 0) == 0)
//			break;
//	}
//	for (i = 0; i < abs(yVelocity); i++)
//	{
//		if (EssaieDeplacement(tileTex, 0, SGN(y)) == 0)
//			break;
//	}
//}
//
//int Player::DeplaceText(Texture* tileTex, int x, int y)
//{
//	if (EssaieDeplacement(tileTex, x, y))
//		return 1;
//
//	Affine(tileTex,x, y);
//	return 2;
//}

void Player::Render(SDL_Rect camera)
{
	SDL_RendererFlip flip = SDL_FLIP_NONE;

	if (!bDirection)
		flip = SDL_FLIP_HORIZONTAL;
	if (!bMoving)
	{
		playerTex->Render(&camera, flip);
	}
	else {
		playerWalking->Render(&camera, flip);
	}
}

//void player::handle_event(int **bg, SDL_Event &e, SDL_Renderer * renderer)
//{
//	if (on_the_ground(bg) && (e.key.keysym.sym == SDLK_UP || e.key.keysym.sym == SDLK_SPACE))
//	{ 
//		setJump();
//		yvel = -up_v * 2;
//	}
//
//	if (e.type == SDL_KEYDOWN && e.key.repeat == 0)
//	{
//		switch (e.key.keysym.sym)
//		{
//		case SDLK_e:
//			 
//			break;
//		case SDLK_LEFT:
//			xvel -= character_VEL;
//			direction = 'l';
//			setMoving(true);
//			break;
//		case SDLK_RIGHT:
//			xvel += character_VEL;
//			direction = 'r';
//			setMoving(true);
//			break;
//
//		case SDLK_SPACE:
//			setJump();
//			break;
//		}
//	}
//	else if (e.type == SDL_KEYUP && e.key.repeat == 0)
//	{
//		// stop moving
//		if (xvel != 0) {
//			switch (e.key.keysym.sym)
//			{
//			case SDLK_LEFT: xvel += character_VEL; setMoving(false); break;
//			case SDLK_RIGHT: xvel -= character_VEL; setMoving(false); break;
//			}
//		}
//	}
//}
//
//void player::move(int ** bg, int WIDTH, int HEIGHT)
//{
//	move_y = 0;
//	if (!on_the_ground(bg)) {
//		// gravity, if not on the ground, 
//		// you pretend to fall
//		move_y = move_y + up_v;
//	}
//	// moving up
//	static int times = 0;
//	if (yvel < 0)
//		// v_y < 0 for jumping
//		times++;
//	if (times >= (jump_high / up_v) || !check_top(bg)) {
//		// when reached jump_high or collision at top
//		// stop jumping
//		// !check_top for top is not empty
//		yvel = 0;
//		times = 0;
//	}
//	move_y = move_y + yvel;
//
//	/*cout << "move_y :" << move_y << "";*/
//	while (move_y != 0) {
//		if (move_y < 0) {
//			// moving up
//			if (check_top(bg)) {
//				box.y = box.y - 1;
//				move_y = move_y + 1;
//			}
//			else
//				break;
//		}
//		else if (move_y > 0) {
//			// moving down
//			if (!on_the_ground(bg) && box.y < HEIGHT) {
//				box.y = box.y + 1;
//				move_y = move_y - 1;
//			}
//			else
//				break;
//		}
//	}
//	move_x = xvel;
//	while (move_x != 0) {
//		if (move_x > 0) {
//			// character go right
//			if (check_right(bg, WIDTH)) {
//				box.x = box.x + 1;
//				move_x = move_x - 1;
//			}
//			else
//				break;
//		}
//		else if (move_x < 0) {
//			// character go left
//			if (check_left(bg)) {
//				box.x = box.x - 1;
//				move_x = move_x + 1;
//			}
//			else
//				break;
//		}
//	}
//
//	/*Animation*/
//	if (moving)
//	{
//		frame += 0.2;
//		if (frame >= 6.4)
//			frame = 0.0;
//	}
//	else
//	{
//		frame = 0.0;
//	}
//
//	// Debut de code pour l'animation du saut
//	//	if (jump)
//	//	{
//	//		frame = 9;
//	//		frame += 0.2;
//	//	}
//
//}
//
//
/////*
////*   When I designed my gravity function I  started with the simple calculation. The resultant downforce on the player is equal to the
////*   force of gravity plus the opposite force (resistance).
////*
////*   Then I had to consider gravity's effect on resistance, or the resistance lessening. So I introduced a terminal velocity for my
////*   character, so that any resistance applied would slowly give in to gravity until it reach terminal velocity.
////*
////*   I then had to consider immovable forces, such as the ground. I added some basic collision detection for the ground's y coordinate
////*   and introduced a boolean for the player being grounded, so gravity could push them no further.
////*   I also set the boolean values for jumping and double jumping to false, so the player can jump again (more on jumping further down)
////*
////*/
////void Player::gravity()
////{
////	if (m_downForce < m_termVel && !m_grounded)
////	{
////		++m_resistance;
////	}
////
////	if (m_rect.y > Game::Get()->getScrHeight() - 100 - m_rect.h)
////	{
////		m_rect.y = Game::Get()->getScrHeight() - 100 - m_rect.h;
////		m_resistance = -m_gravForce;
////		m_jumping = m_doubleJumping = false;
////		m_grounded = true;
////	}
////
////	m_downForce = m_gravForce + m_resistance;
////
////	m_vel.setY(m_downForce);
////}
////
/////*
////*   This is my jump function. The sequence of if statements using the jumping and double jumping boolean variables, only allows the
////*   player to perform each action (jump and double jump) if they have not already performed it and completed it (hit the ground).
////*
////*   When either action is performed, the player is no longer grounded, I apply the force of the player's jump to the resistance and set
////*   the resultant down force to make the character jump.
////*/
////void Player::jump()
////{
////	if (!m_doubleJumping)
////	{
////		if (!m_jumping)
////		{
////			m_jumping = true;
////		}
////		else
////		{
////			m_doubleJumping = true;
////		}
////		m_grounded = false;
////		m_resistance = m_jumpForce;
////		m_downForce = m_gravForce + m_resistance;
////	}
////}#include "Player.h"

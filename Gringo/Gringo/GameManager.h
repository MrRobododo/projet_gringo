#pragma once
#ifndef _GAMEMANAGER_H
#define _GAMEMANAGER_H

#include "AudioManager.h"
#include "ScreenManager.h"

namespace SDL {
	class GameManager
	{
	private:
		static GameManager*instance;
		bool mQuit;

		const int FRAME_RATE = 60;

		InputManager * mInputManager;
		AudioManager * mSound;


		Graphics * mGraphics;
		AssetsManager * mAssetsManager;
		//AnimatedTexture * mTex;

		Texture * TX;
		//Texture *t1;

		Timer * mTimer;

		SDL_Event mEvent;

		ScreenManager * mScreenManager;


	public:

		static GameManager* Instance();
		static void Release();

		void Run();

	private:
		GameManager();
		~GameManager();

		void EarlyUpdate();
		void Update();
		void LateUpdate();

		void Render();
	};

}
#endif // !1
